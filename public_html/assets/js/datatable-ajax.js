
$(document).ready(function () {

    $('.datatable-ajax').each(function () {
        var tb = $(this);
        var url = $(this).attr('data-url');
        var filter = $(this).attr('data-filter');
        var orderColumn = $(this).attr('data-order-column');
        var orderType = $(this).attr('data-order-type');
        var options = $(this).find('th.options').attr('data-html');
        var customSearch = $(this).attr('data-custom-search');
        var columnDefs = [];
        if (options) {
            columnDefs = [{
                "targets": -1,
                "data": null,
                "defaultContent": $(options).html()
            }];
        }
        var filter_s = "";
        if (filter) {
            filter_s = $(filter).serialize();
        }

        var options = {
            "order": [[ (orderColumn ? orderColumn : '0'), (orderType ? orderType : 'desc') ]],
            "searching": (customSearch == 1 ? false : true),
            "lengthChange": false,
            "processing": true,
            "serverSide": true,
            "ajax": url+"?"+filter_s,
            "rowId": "id",
            "columnDefs": columnDefs,
            "autoWidth": false,
            "language": {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ registros",
                "sZeroRecords": "No se encontraron resultados",
                "sEmptyTable": "Ningún dato disponible en esta tabla.",
                "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copy": "Copiar",
                    "colvis": "Visibilidad"
                }
            }
        };

        var dt = $(this).DataTable(options);

        dt.on('page.dt', function () {
            /*
            $('html, body').animate({
                //scrollTop: tb.offset().top - 160
                scrollTop: 0
            }, 'fast');
            */
            $('html, body').scrollTop(tb.offset().top - 170);
        });

        if(filter) {
            
            $(document).on('submit', filter, function(){
                var params = $(this).serialize();
                dt.ajax.url(url+'?'+params).load();
                return false;
            });
        }
            
        $(document).on('click', '.btn-refresh', function(){
            dt.ajax.reload(null, false);
            return false;
        });
        
    });

});