$(function () {

    $('body').tooltip({selector:'[data-toggle=tooltip]'});

    $(document).on('submit','.bform',function(){
        frm = $(this);
        btn = $(this).find('.btn-save');
        route = frm.attr("data-route");
        msg = frm.attr("data-msg") ? frm.attr("data-msg"): '';
        fn = frm.attr("data-fn") ? frm.attr("data-fn") : '';
        
        btn.attr("disabled","disabled");
        
        $.ajax(
        {
            url: frm.attr('action'),
            type: "POST",
            data: frm.serialize()
        })
        .done(function(data)
        {
            btn.removeAttr('disabled');
            if(data.success==1){
                $('.modal').modal('hide');
                if(fn != ''){
                    alertify.alert('Aviso', 'Datos guardados', eval(fn));
                }else{
                    alertify.alert('Aviso', 'Datos guardados');
                    location.reload();
                }
            }else{
                alertify.alert('Aviso', data.data.join('<br>'));
            }
        })
        .fail(function(j,t,e){
            btn.removeAttr('disabled');
            errorEvent(j,t,e);
        });

        $('input,select,button').blur();
        return false;
    });
    
    $('.selectpicker').selectpicker();

    $('.selectpicker.with-ajax').each(function(){
        var path = $(this).attr('data-url');
        $(this).selectpicker().ajaxSelectPicker({
            ajax          : {
                url     : site_url+'/'+path,
                type    : 'POST',
                dataType: 'json',
                // Use "{{{q}}}" as a placeholder and Ajax Bootstrap Select will
                // automatically replace it with the value of the search query.
                data    : {
                    q: '{{{q}}}'
                }
            },
            langCode : 'es-ES',
            log           : 3,
            preprocessData: function (data) {
                var i, l = data.data.length, array = [];
                if (l) {
                    for (i = 0; i < l; i++) {
                        array.push($.extend(true, data.data[i], {
                            text : data.data[i].name,
                            value: data.data[i].id
                        }));
                    }
                }
                // You must always return a valid array when processing data. The
                // data argument passed is a clone and cannot be modified directly.
                return array;
            }
        });
    });
    

});

/* Extra */

function errorEvent(jqXHR, textStatus, errorThrown) {
    var reload = 0;
    var msg = '';
    if (jqXHR.status === 0) {
        msg = 'Not connect: Verify Network.';
    } else if (jqXHR.status == 401) {
        reload = 1;
    } else if (jqXHR.status == 404) {
        msg = 'Requested page not found [404]';
    } else if (jqXHR.status == 500) {
        msg = 'Internal Server Error [500].';
    } else if (textStatus === 'parsererror') {
        msg = 'Requested JSON parse failed.';
    } else if (textStatus === 'timeout') {
        msg = 'Time out error.';
    } else if (textStatus === 'abort') {
        msg = 'Ajax request aborted.';
    } else {
        msg = 'Uncaught Error: ' + jqXHR.responseText;
    }
    
    if(reload == 1){
        location.reload();
    } else {
        alert(msg);
    }
}