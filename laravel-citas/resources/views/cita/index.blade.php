@extends('layouts.app')

@section('title', 'Citas')

@section('lateral-top')
    <div class="d-flex justify-content-end align-items-center">
        <button type="button" data-toggle="modal" data-target="#form-modal" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Registrar</button>
    </div>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <form id="form-filter">
                    <select class="form-control" name="estado" style="width: 130px">
                        <option value="">Todos</option>
                        @foreach($estado as $e)
                        @php ($selected = $loop->index == 0 ? 'selected="selected"':'')
                        <option value="{!! $e->id !!}" {!! $selected !!}> {!! $e->nombre !!}</option>
                        @endforeach
                    </select>
                    <button class="btn btn-success" id="filter-btn">Buscar</button>
                </form>
                <table data-url="{!! url('cita/list') !!}"  data-custom-search="1" data-filter="#form-filter" data-page-length="25" class="datatable-ajax table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th data-data="dia">Día</th>
                            <th data-data="hora_inicio">Inicio</th>
                            <th data-data="hora_fin">Fin</th>
                            <th data-data="estado">Estado</th>
                            <th data-data="paciente">Paciente</th>
                            <th data-data="medico">Medico</th>
                            <th data-data="options" data-sortable="false">Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="form-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Registrar cita</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form class="bform" action="{{ url('cita/insert') }}" autocomplete="off" data-fn="recargar()">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Paciente:</label>
                        <select class="form-control selectpicker with-ajax" name="paciente_id" data-live-search="true" data-url="cita/paciente"></select>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Sede:</label>
                                <select class="form-control" name="sede_id" id="se-sede">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Especialidad:</label>
                                <select class="form-control" name="especialidad_id" id="se-especialidad">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Médico:</label>
                                <select class="form-control" name="medico_id" id="se-medico">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Día:</label>
                                    <input type="text" class="form-control" name="fecha" id="in-fecha" value="{{ date('d/m/Y') }}" readonly="readonly">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Hora:</label>
                                <select class="form-control" name="hora" id="se-hora">
                                    <option value="">Seleccione</option>
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="importe" value="0">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-save btn-danger waves-effect waves-light">Registrar</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/bootstrap-select/css/bootstrap-select.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/Ajax-Bootstrap-Select/dist/css/ajax-bootstrap-select.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('js')
    <script src="{{ asset('assets/plugins/Ajax-Bootstrap-Select/dist/js/ajax-bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/Ajax-Bootstrap-Select/dist/js/locale/ajax-bootstrap-select.es-ES.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>

    <script type="text/javascript">
    /* Date */

    var route = site_url+'/cita';
    
    $('#in-fecha').datepicker({
        startDate: "-",
        todayBtn: "linked",
        language: "es",
        autoclose: true
    })
    .on('changeDate', cambioMedico);
    
    function recargar(){
        $('#filter-btn').trigger('click');
    }
        
    $(document).on('click', '.btn-check', function(){
        id = $(this).parent().parent().attr('id');
        btn = $(this);

        alertify.confirm('Aviso', '¿Desea marcar como atenido?', function(){
            btn.attr('disabled', 'disabled');
            $.ajax({
                type: "GET",
                dataType: "json",
                url: route+'/action/check/'+id
            })
            .done(function( data ) {
                if(data.success == 1){
                    recargar();
                }
            })
            .fail(function(j,t,e){errorEvent(j,t,e)})
            .then(function() {
                btn.removeAttr('disabled');
            });
        }, null);
    });

    $(document).on('click', '.btn-delete', function(){
        id = $(this).parent().parent().attr('id');
        btn = $(this);

        alertify.confirm('Aviso', '¿Desea anular esta cita?', function(){
            btn.attr('disabled', 'disabled');
            $.ajax({
                type: "GET",
                dataType: "json",
                url: route+'/action/delete/'+id
            })
            .done(function( data ) {
                if(data.success == 1){
                    recargar();
                }
            })
            .fail(function(j,t,e){errorEvent(j,t,e)})
            .then(function() {
                btn.removeAttr('disabled');
            });
        }, null);
    });
        
    /* Select */

    cargarSelect(route+'/sede', '#se-sede', '');
    
    $('#se-sede').on('change', function(){
        var id = $(this).val();
        if(id == ''){
            resetSelect('#se-especialidad');
        }else{
            cargarSelect(route+'/especialidad/'+id, '#se-especialidad', '');
        }
    });

    $('#se-especialidad').on('change', function(){
        var id = $(this).val();
        var idEspecialidad = $('#se-especialidad').val();
        if(id == ''){
            resetSelect('#se-medico');
        }else{
            cargarSelect(route+'/medico/'+id+'/'+idEspecialidad, '#se-medico', '');
        }
    });

    $('#se-medico').on('change', cambioMedico);
    
    function cambioMedico(){
        var id = $('#se-medico').val();
        var idSede = $('#se-sede').val();
        var idEspecialidad = $('#se-especialidad').val();
        var idMedico = $('#se-medico').val();
        var fecha = $('#in-fecha').val();
        if(id == '' || fecha == ''){
            resetSelect('#se-hora');
        }else{
            fecha = fecha.split("/").reverse().join("-");
            cargarSelect(route+'/hora/'+idSede+'/'+idEspecialidad+'/'+id+'/'+fecha, '#se-hora', '');
        }
    }

    function cargarSelect(url, select, opSelected){
        $.ajax({
            type: "GET",
            dataType: "json",
            url: url
        })
        .done(function( data, textStatus, jqXHR ) {
            if(data.success == 1){
                var array = data.data;
                resetSelect(select);
                for (var i = 0; i < array.length; i++) {
                    $(select).append('<option value="'+array[i].id+'">'+array[i].nombre+'</option>');
                }
                $(select).val(opSelected);
            }
        })
        .fail(function( jqXHR, textStatus, errorThrown ) {
            
        });
    }

    function resetSelect(select){
        var option = $(select).find('option:eq(0)').html();
        $(select).html('');
        $(select).append('<option value="">'+option+'</option>');
    }
    </script>
@endsection