@extends('layouts.app')

@section('title', 'Turnos')

@section('lateral-top')

@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <button class="btn-refresh hide">Recargar</button>
                <table data-url="{!! url('turno/list') !!}" data-page-length="25" data-order-type="asc" class="datatable-ajax table table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th data-data="nombre">Nombre</th>
                            <th data-data="apellido_pa">Apellido Paterno</th>
                            <th data-data="apellido_ma">Apellido Materno</th>
                            <th data-data="cargo">Cargo</th>
                            <th data-data="n_documento">N° Documento</th>
                            <th data-data="options" data-sortable="false" data-width="50px">Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="form-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><span id="action-form"></span> Turno</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form class="bform" action="" autocomplete="off" data-fn="recargar()">
                @csrf
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-save btn-danger waves-effect waves-light">Guardar</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <div id="form-body" class="hide">
        <input class="form-control" name="id" value="" type="hidden">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Sede:</label>
                    <select class="form-control" name="sede">
                        <option value="">Seleccione</option>
                        @foreach($sede as $s)
                        <option value="{{ $s->id }}">{{ $s->nombre }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Especialidad:</label>
                    <select class="form-control" name="especialidad">
                        <option value="">Seleccione</option>
                        @foreach($especialidad as $s)
                        <option value="{{ $s->id }}">{{ $s->nombre }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Días:</label>
                    <select class="form-control" name="dia">
                        <option value="">Seleccione</option>
                        @foreach($dias as $d => $dia)
                        <option value="{{ $d }}">{{ $dia }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Desde:</label>
                    <select class="form-control" name="desde">
                        <option value="">Seleccione</option>
                        @for ($i = 6; $i < 24; $i++)
                            @php ($h = sprintf('%02d', $i));
                            <option value="{{ $h }}:00">{{ $h }}:00</option>
                            <option value="{{ $h }}:30">{{ $h }}:30</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Hasta:</label>
                    <select class="form-control" name="hasta">
                        <option value="">Seleccione</option>
                        @for ($i = 6; $i < 24; $i++)
                            @php ($h = sprintf('%02d', $i));
                            <option value="{{ $h }}:00">{{ $h }}:00</option>
                            <option value="{{ $h }}:30">{{ $h }}:30</option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="col-sm-4">
                <button class="btn btn-turno-add btn-info btn-block" style="margin-top:30px" type="button"><i class="fas fa-plus"></i> Agregar</button>
            </div>
            <div class="col-sm-12">
                <table class="table table-sm table-turno">
                    <tr>
                        <th>Sede</th>
                        <th>Especialidad</th>
                        <th>Día</th>
                        <th>Desde</th>
                        <th>Hasta</th>
                        <th style="width:50px">Opciones</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    
    <!-- Modal -->
    <div id="form-tarifa-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tarifa</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form class="bform" action="" autocomplete="off" data-fn="recargar()">
                @csrf
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-save btn-danger waves-effect waves-light">Guardar</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <div id="form-tarifa-body" class="hide">
        <input class="form-control" name="id" value="" type="hidden">
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-sm table-tarifa">
                    <tr>
                        <th>Especialidad</th>
                        <th>Sede</th>
                        <th>Costo</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript">
    $(function(){

        var route = site_url+'/turno';
        
        function resetForm(action, url){
            $('#form-modal .modal-body').html($('#form-body').html());
            $('#form-modal form').attr('action', route + '/'+ url);
            $('#action-form').html(action);
        }
        
        function resetTarifaForm(action, url){
            $('#form-tarifa-modal .modal-body').html($('#form-tarifa-body').html());
            $('#form-tarifa-modal form').attr('action', route + '/'+ url);
        }

        $(document).on('click', '.btn-tarifa', function(){
            resetTarifaForm('Registrar', 'update-tarifa');
            $('#form-tarifa-modal').modal('show');

            id = $(this).parent().parent().attr('id');

            $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: route+'/info-tarifa/'+id
                })
                .done(function( data ) {
                    if(data.success == 1){
                        $('input[name="id"]').val(id);

                        var lista = data.data['detalle'];
                        for (var i = 0; i < lista.length; i++) {
                            const element = lista[i];

                            var sede_v = lista[i]['sede_id'];
                            var especialidad_v = lista[i]['especialidad_id'];

                            var row = ""+
                            "<tr>"+
                            "<td>"+lista[i]['sede']+"<input type='hidden' name='d_sede[]' value='"+sede_v+"'></td>"+
                            "<td>"+lista[i]['especialidad']+"<input type='hidden' name='d_especialidad[]' value='"+especialidad_v+"'></td>"+
                            "<td><input type='text' class='form-control' style='width:50px' name='importe[]' value='"+lista[i]['importe']+"'></td>"+
                            "</tr>";
                            $(document).find('#form-tarifa-modal .table-tarifa').append(row);
                        }
            
                    }
                })
                .fail(function(j,t,e){errorEvent(j,t,e)})
                .then(function() {
                    btn.removeAttr('disabled');
                });

        });
        
        $(document).on('click', '.btn-edit', function(){
            resetForm('Modificar', 'update-turno');
            $('#form-modal').modal('show');

            id = $(this).parent().parent().attr('id');

            $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: route+'/info/'+id
                })
                .done(function( data ) {
                    if(data.success == 1){
                        $('input[name="id"]').val(id);
                        var dias = ["Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"];
                        var lista = data.data['detalle'];
                        for (var i = 0; i < lista.length; i++) {
                            const element = lista[i];

                            var sede_v = lista[i]['sede_id'];
                            var especialidad_v = lista[i]['especialidad_id'];
                            var dia_v = lista[i]['dia'];
                            var desde_v = lista[i]['hora_inicio'];
                            var hasta_v = lista[i]['hora_fin'];

                            var clase = "tb-"+sede_v+"-"+especialidad_v+"-"+dia_v+"-"+desde_v+"-"+hasta_v;

                            var row = ""+
                            "<tr class='"+clase+"'>"+
                            "<td>"+lista[i]['sede']+"<input type='hidden' name='d_sede[]' value='"+sede_v+"'></td>"+
                            "<td>"+lista[i]['especialidad']+"<input type='hidden' name='d_especialidad[]' value='"+especialidad_v+"'></td>"+
                            "<td>"+dias[lista[i]['dia']-1]+"<input type='hidden' name='d_dia[]' value='"+dia_v+"'></td>"+
                            "<td>"+lista[i]['hora_inicio']+"<input type='hidden' name='d_desde[]' value='"+desde_v+"'></td>"+
                            "<td>"+lista[i]['hora_fin']+"<input type='hidden' name='d_hasta[]' value='"+hasta_v+"'></td>"+
                            "<td><button class='btn btn-delete-item btn-sm btn-danger'><i class='fas fa-trash'></i></button></td>"+
                            "</tr>";
                            $(document).find('#form-modal .table-turno').append(row);
                        }
            
                    }
                })
                .fail(function(j,t,e){errorEvent(j,t,e)})
                .then(function() {
                    btn.removeAttr('disabled');
                });
            
        });

        $(document).on('click', '.btn-delete', function(){
            id = $(this).parent().parent().attr('id');
            btn = $(this);

            alertify.confirm('Aviso', '¿Desea eliminar este elemento?', function(){
                btn.attr('disabled', 'disabled');
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: route+'/delete/'+id
                })
                .done(function( data ) {
                    if(data.success == 1){
                        recargar();
                    }
                })
                .fail(function(j,t,e){errorEvent(j,t,e)})
                .then(function() {
                    btn.removeAttr('disabled');
                });
            }, null);
        });
        
        $(document).on('click', '.btn-turno-add', function(){
            frm = $(this).parent().parent();
            var sede = frm.find('[name="sede"] option:selected');
            var especialidad = frm.find('[name="especialidad"] option:selected');
            var dia = frm.find('[name="dia"] option:selected');
            var desde = frm.find('[name="desde"] option:selected');
            var hasta = frm.find('[name="hasta"] option:selected');
            
            var sede_v = sede.attr('value');
            var especialidad_v = especialidad.attr('value');
            var dia_v = dia.attr('value');
            var desde_v = desde.attr('value');
            var hasta_v = hasta.attr('value');
            
            var clase = "tb-"+sede_v+"-"+especialidad_v+"-"+dia_v+"-"+desde_v+"-"+hasta_v;

            var row = ""+
            "<tr class='"+clase+"'>"+
            "<td>"+sede.text()+"<input type='hidden' name='d_sede[]' value='"+sede_v+"'></td>"+
            "<td>"+especialidad.text()+"<input type='hidden' name='d_especialidad[]' value='"+especialidad_v+"'></td>"+
            "<td>"+dia.text()+"<input type='hidden' name='d_dia[]' value='"+dia_v+"'></td>"+
            "<td>"+desde.text()+"<input type='hidden' name='d_desde[]' value='"+desde_v+"'></td>"+
            "<td>"+hasta.text()+"<input type='hidden' name='d_hasta[]' value='"+hasta_v+"'></td>"+
            "<td><button class='btn btn-delete-item btn-sm btn-danger'><i class='fas fa-trash'></i></button></td>"+
            "</tr>";
            
            if(!frm.find(".table-turno tr").hasClass(clase) && desde_v != hasta_v && sede_v != '' && especialidad_v != '' && dia_v != '' && desde_v != '' && hasta_v != ''){
                frm.find(".table-turno").append(row);
            }
        });

        $(document).on('click', '.btn-delete-item', function(){
            $(this).parent().parent().remove();
        });
    });
    </script>
    <script type="text/javascript">
    function recargar(){
        $('.btn-refresh').trigger('click');
    }
    </script>
@endsection
