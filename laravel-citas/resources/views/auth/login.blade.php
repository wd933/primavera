@extends('layouts.auth')

@section('content')
        <form class="form-horizontal form-material" id="loginform" method="POST" action="{{ route('login') }}">
        @csrf
        <div class="text-center" style="margin: 20px 0">
            <img src="{{ asset('assets/images/logo.png') }}" style="width: 100%; max-width: 200px;">
        </div>
        <div class="form-group ">
            <div class="col-xs-12">
                <input id="email" type="email" placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                    <label class="custom-control-label" for="customCheck1">Recordarme</label>
                    <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> No recuerdo mi contraseña.</a> 
                </div> 
            </div>
        </div>
        <div class="form-group text-center">
            <div class="col-xs-12 p-t-20 p-b-20">
                <button class="btn btn-block btn-lg btn-info btn-rounded" type="submit">Ingresar</button>
            </div>
        </div>
        <!--
        <div class="form-group m-b-0">
            <div class="col-sm-12 text-center">
                Don't have an account? <a href="pages-register.html" class="text-info m-l-5"><b>Sign Up</b></a>
            </div>
        </div>
        -->
    </form>
@endsection
