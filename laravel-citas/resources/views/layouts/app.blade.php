<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/template/eliteadmin-40/assets/images/favicon.png') }}">
    <title>Sistema</title>
    <!-- Alertify -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/alertify/css/alertify.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/plugins/alertify/css/themes/default.min.css') }}">
    <!-- Fontawesome -->
    <link href="{{ asset('assets/node_modules/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    @yield('css')
    <style type="text/css">
        .datatable-ajax th{padding:8px}
        .datatable-ajax td{padding:8px}
    </style>
    <!-- Custom CSS -->
    <link href="{{ asset('assets/template/eliteadmin-40/hospital/dist/css/style.min.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="skin-megna fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Primavera</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="{{ asset('assets/template/eliteadmin-40/assets/images/logo-icon.png') }}" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="{{ asset('assets/template/eliteadmin-40/assets/images/logo-light-icon.png') }}" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <span class="hidden-xs"><span class="font-bold">Primavera</span></span>
                    </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler d-block d-sm-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                        <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                    </ul>
                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- User Profile -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown u-pro">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="hidden-md-down">Mi Cuenta &nbsp;<i class="fa fa-angle-down"></i></span> </a>
                            <div class="dropdown-menu dropdown-menu-right animated flipInY">
                                <!-- text-->
                                <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-power-off"></i> Salir</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                <!-- text-->
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <!--
                        <li> <a class="waves-effect waves-dark" href="{{ url('agenda') }}"><i class="icon-speedometer"></i><span class="hide-menu">Agenda</span></a>
                        </li>
                        -->
                        <li> <a class="waves-effect waves-dark" href="{{ url('cita') }}"><i class="ti-layout-grid2"></i><span class="hide-menu">Citas</span></a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{ url('paciente') }}"><i class="ti-layout-grid2"></i><span class="hide-menu">Pacientes</span></a>
                        </li>
                        <!--
                        <li> <a class="waves-effect waves-dark" href="{{ url('medico') }}"><i class="ti-layout-grid2"></i><span class="hide-menu">Médicos</span></a>
                        </li>
                        -->
                        <li> <a class="waves-effect waves-dark" href="{{ url('trabajador') }}"><i class="ti-layout-grid2"></i><span class="hide-menu">Trabajadores</span></a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{ url('sede') }}"><i class="ti-layout-grid2"></i><span class="hide-menu">Sedes</span></a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{ url('especialidad') }}"><i class="ti-layout-grid2"></i><span class="hide-menu">Especialidades</span></a>
                        </li>
                        <li> <a class="waves-effect waves-dark" href="{{ url('turno') }}"><i class="ti-layout-grid2"></i><span class="hide-menu">Turnos</span></a>
                        </li>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">@yield('title')</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        @yield('lateral-top')
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        @yield('content')
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © Todos los derechos reservados
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->

    <!-- All JS -->
    
    <script type="text/javascript">var site_url = '{{ url("/") }}';</script>
    <!-- Fontawesome -->
    <script src="{{ asset('assets/node_modules/@fortawesome/fontawesome-free/js/all.min.js') }}"></script>
    <!-- jQuery -->
    <script src="{{ asset('assets/template/eliteadmin-40/assets/node_modules/jquery/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="{{ asset('assets/template/eliteadmin-40/assets/node_modules/popper/popper.min.js') }}"></script>
    <script src="{{ asset('assets/template/eliteadmin-40/assets/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="{{ asset('assets/template/eliteadmin-40/hospital/dist/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <!-- Wave Effects -->
    <script src="{{ asset('assets/template/eliteadmin-40/hospital/dist/js/waves.js') }}"></script>
    <!-- Menu sidebar -->
    <script src="{{ asset('assets/template/eliteadmin-40/hospital/dist/js/sidebarmenu.js') }}"></script>
    <!-- stickey kit -->
    <script src="{{ asset('assets/template/eliteadmin-40/assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js') }}"></script>
    <script src="{{ asset('assets/template/eliteadmin-40/assets/node_modules/sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- Custom JavaScript -->
    <script src="{{ asset('assets/template/eliteadmin-40/hospital/dist/js/custom.min.js') }}"></script>
    
    <!-- Alertify -->
    <script src="{{ asset('assets/plugins/alertify/alertify.min.js') }}"></script>
    <!-- DataTables -->
    <script src="{{ asset('assets/template/eliteadmin-40/assets/node_modules/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/datatable-ajax.js?v='.time()) }}"></script>
    
    <!-- Functions -->
    <script src="{{ asset('assets/js/functions.js') }}"></script>
    @yield('js')
</body>

</html>