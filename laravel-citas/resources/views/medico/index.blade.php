@extends('layouts.app')

@section('title', 'Médicos')

@section('lateral-top')
    <div class="d-flex justify-content-end align-items-center">
        <button type="button" data-toggle="modal" data-target="#form-modal" class="btn btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Registrar</button>
    </div>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <button class="btn-refresh hide">Recargar</button>
                <table data-url="{!! url('medico/list') !!}" data-page-length="25" class="datatable-ajax table table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th data-data="nombre">Nombre</th>
                            <th data-data="apellido_pa">Apellido Paterno</th>
                            <th data-data="apellido_ma">Apellido Materno</th>
                            <th data-data="nombre">Especialidad</th>
                            <th data-data="n_documento">N° Documento</th>
                            <th data-data="options" data-sortable="false">Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="form-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Registro Médico</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form class="bform" action="{{ url('cita/insert') }}" autocomplete="off" data-fn="recargar()">
                @csrf
                <div class="modal-body">
                    <div class="row">

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Nombre:</label>
                                <input class="form-control" name="nombre">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Apellido Paterno:</label>
                                <input class="form-control" name="apellido_pa">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Apellido Materno:</label>
                                <input class="form-control" name="apellido_ma">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Identificación:</label>
                                <select name="identificacion_id" class="form-control">
                                    <option value="1">DNI</option>
                                    <option value="2">CE</option>
                                    <option value="3">Pasaporte</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">N° Documento:</label>
                                <input class="form-control" name="n_documento">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Usuario:</label>
                                <input class="form-control" name="usuario">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Contraseña:</label>
                                <input class="form-control" name="password" type="password">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-save btn-danger waves-effect waves-light">Guardar</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->
@endsection

@section('js')
    <script type="text/javascript">
    /* Date */

    var route = site_url+'/cita';
    
    function recargar(){
        $('.btn-refresh').trigger('click');
    }
        
    $(document).on('click', '.btn-edit', function(){
        id = $(this).parent().parent().attr('id');
        btn = $(this);

        $('#form-modal').modal('show');
        //$('.btn-refresh').trigger('click');
    });

    $(document).on('click', '.btn-delete', function(){
        id = $(this).parent().parent().attr('id');
        btn = $(this);

        alertify.confirm('Aviso', '¿Desea eliminar este elemento?', function(){
            btn.attr('disabled', 'disabled');
            $.ajax({
                type: "GET",
                dataType: "json",
                url: route+'/action/delete/'+id
            })
            .done(function( data ) {
                if(data.success == 1){
                    recargar();
                }
            })
            .fail(function(j,t,e){errorEvent(j,t,e)})
            .then(function() {
                btn.removeAttr('disabled');
            });
        }, null);
    });
    </script>
@endsection