@extends('layouts.app')

@section('title', 'Agenda')

@section('lateral-top')
@endsection

@section('content')

<div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('css')
    <!-- Calendar CSS -->
    <link href="{{ asset('assets/template/eliteadmin-40/assets/node_modules/calendar/dist/fullcalendar.css') }}" rel="stylesheet" />
@endsection

@section('js')
    <!-- Calendar JavaScript -->
    <script src="{{ asset('assets/template/eliteadmin-40/assets/node_modules/calendar/dist/lang/es.js') }}"></script>
    <script src="{{ asset('assets/template/eliteadmin-40/assets/node_modules/calendar/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/template/eliteadmin-40/assets/node_modules/moment/moment.js') }}"></script>
    <script src="{{ asset('assets/template/eliteadmin-40/assets/node_modules/calendar/dist/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('assets/template/eliteadmin-40/assets/node_modules/calendar/dist/cal-init.js') }}"></script>
@endsection