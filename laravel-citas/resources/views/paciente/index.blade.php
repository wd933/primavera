@extends('layouts.app')

@section('title', 'Pacientes')

@section('lateral-top')
    <div class="d-flex justify-content-end align-items-center">
        <button type="button" class="btn btn-add btn-info d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Registrar</button>
    </div>
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <button class="btn-refresh hide">Recargar</button>
                <table data-url="{!! url('paciente/list') !!}" data-page-length="25" data-order-type="asc" class="datatable-ajax table table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th data-data="nombre">Nombre</th>
                            <th data-data="apellido_pa">Apellido Paterno</th>
                            <th data-data="apellido_ma">Apellido Materno</th>
                            <th data-data="n_documento">N° Documento</th>
                            <th data-data="options" data-sortable="false" data-width="50px">Opciones</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="form-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><span id="action-form"></span> Paciente</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form class="bform" action="" autocomplete="off" data-fn="recargar()">
                @csrf
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-save btn-danger waves-effect waves-light">Guardar</button>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.modal -->

    <div id="form-body" class="hide">
        <input class="form-control" name="id" value="" type="hidden">
        <div class="row">

            <div class="col-sm-12">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Nombre:</label>
                    <input class="form-control" name="nombre">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Apellido Paterno:</label>
                    <input class="form-control" name="apellido_pa">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Apellido Materno:</label>
                    <input class="form-control" name="apellido_ma">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Identificación:</label>
                    <select class="form-control" name="identificacion_id">
                        <option value="">Seleccione</option>
                        @foreach($identificacion as $o)
                        <option value="{{ $o->id }}">{{ $o->nombre }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">N° Documento:</label>
                    <input class="form-control" name="n_documento">
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Dirección:</label>
                    <input class="form-control" name="direccion">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Celular:</label>
                    <input class="form-control" name="celular">
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="recipient-name" class="control-label">Teléfono:</label>
                    <input class="form-control" name="telefono">
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
    $(function(){

        var route = site_url+'/paciente';
        
        function resetForm(action, url){
            $('#form-modal .modal-body').html($('#form-body').html());
            $('#form-modal form').attr('action', route + '/'+ url);
            $('#action-form').html(action);
        }

        $(document).on('click', '.btn-add', function(){
            resetForm('Registrar', 'insert');
            $('#form-modal').modal('show');
        });
        
        $(document).on('click', '.btn-edit', function(){
            resetForm('Modificar', 'update');
            $('#form-modal').modal('show');

            id = $(this).parent().parent().attr('id');
            $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: route+'/info/'+id
                })
                .done(function( data ) {
                    if(data.success == 1){
                        $('input[name="id"]').val(data.data['id']);
                        $('input[name="nombre"]').val(data.data['nombre']);
                        $('input[name="apellido_pa"]').val(data.data['apellido_pa']);
                        $('input[name="apellido_ma"]').val(data.data['apellido_ma']);
                        $('select[name="identificacion_id"]').val(data.data['identificacion_id']);
                        $('input[name="n_documento"]').val(data.data['n_documento']);
                        $('input[name="direccion"]').val(data.data['direccion']);
                        $('input[name="celular"]').val(data.data['celular']);
                        $('input[name="telefono"]').val(data.data['telefono']);
                    }
                })
                .fail(function(j,t,e){errorEvent(j,t,e)})
                .then(function() {
                    btn.removeAttr('disabled');
                });
            
        });

        $(document).on('click', '.btn-delete', function(){
            id = $(this).parent().parent().attr('id');
            btn = $(this);

            alertify.confirm('Aviso', '¿Desea eliminar este elemento?', function(){
                btn.attr('disabled', 'disabled');
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: route+'/delete/'+id
                })
                .done(function( data ) {
                    if(data.success == 1){
                        recargar();
                    }
                })
                .fail(function(j,t,e){errorEvent(j,t,e)})
                .then(function() {
                    btn.removeAttr('disabled');
                });
            }, null);
        });

    });
    </script>
    <script type="text/javascript">
    function recargar(){
        $('.btn-refresh').trigger('click');
    }
    </script>
@endsection