<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('login', 'Api\AuthController@loginUser');
Route::post('login', 'Api\AuthController@loginUser');
Route::get('cita', 'Admin\CitaController@test');
Route::get('cita/sede', 'Admin\CitaController@getSede');
Route::get('cita/especialidad/{idSede}', 'Admin\CitaController@getEspecialidad');
Route::get('cita/medico/{idSede}/{idEspecialidad}', 'Admin\CitaController@getMedico');
Route::get('cita/medico-lista', 'Admin\CitaController@getMedicoLista');
Route::get('cita/horario/{idSede}/{idEspecialidad}/{idMedico}/{fecha}', 'Admin\CitaController@getHora');
Route::get('cita/listado/{idPaciente}', 'Admin\CitaController@listado');
Route::post('cita/insert', 'Admin\CitaController@postInsert');
Route::get('cita/{id}', 'Admin\CitaController@getInfo');
Route::get('cita/action/{type}/{id}', 'Admin\CitaController@getAction');
Route::post('paciente/registro', 'Admin\PacienteController@registro');
Route::get('medico/listado', 'Admin\MedicoController@listado');