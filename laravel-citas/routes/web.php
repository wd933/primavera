<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::group(['middleware' => ['auth']], function () {

    AdvancedRoute::controller('/paciente', 'Admin\PacienteController');
    AdvancedRoute::controller('/trabajador', 'Admin\TrabajadorController');
    AdvancedRoute::controller('/medico', 'Admin\MedicoController');
    AdvancedRoute::controller('/cita', 'Admin\CitaController');
    AdvancedRoute::controller('/especialidad', 'Admin\EspecialidadController');
    AdvancedRoute::controller('/sede', 'Admin\SedeController');
    AdvancedRoute::controller('/turno', 'Admin\TurnoController');

    Route::get('/home', function () {
        return redirect('cita');
    });

    /* Rutas */
    Route::get('/login', function () {
        return view('login');
    });
    Route::get('/agenda', function () {
        return view('agenda/index');
    });
});

Auth::routes(['register' => false]);