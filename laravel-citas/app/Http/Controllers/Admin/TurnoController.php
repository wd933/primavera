<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Especialidad;
use App\Models\Sede;
use App\Models\Trabajador;
use App\Models\Turno;
use App\Models\Tarifario;

class TurnoController extends Controller
{
    public function getIndex()
    {
        $data = [
            'sede' => Sede::active()->select('id', 'nombre')->get(),
            'especialidad' => Especialidad::active()->select('id', 'nombre')->get(),
            'dias' => [1 => 'Lunes', 2 => 'Martes', 3 => 'Miércoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sábado', 7 => 'Domingo']
        ];
        return view('turno.index', $data);
    }

    public function getList()
    {
        $sql = Trabajador::listaTabla(2);

        return dataTables()->of($sql)
            ->addColumn('options', '
                <button class="btn btn-edit btn-sm btn-info"><i class="fas fa-calendar-alt"></i></button>    
                <button class="btn btn-tarifa btn-sm btn-success"><i class="fas fa-money-bill-wave-alt"></i></button>
            ')
            ->rawColumns(['options'])->make(true);
    }

    public function getForm()
    {
        return response()->json([
            'success' => 1,
            'data' => [
            ]
        ]);
    }

    public function postUpdateTarifa(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'importe' => 'required|array|between:0,150'
        ]);
        
        if (!$validator->fails()) {
            $trabajador_id = $request->id;
            $sede = $request->d_sede;
    
            for ($i=0; $i < count($sede); $i++) {
                $insert = Tarifario::updateOrCreate([
                    'trabajador_id' => $trabajador_id,
                    'sede_id' => $request->d_sede[$i],
                    'especialidad_id' => $request->d_especialidad[$i],
                    'cita_motivo_id' => 1, // Todo: cambiar
                    'trabajador_id' => $trabajador_id
                ], [
                    'importe' => $request->importe[$i]
                ]);
            }
            return response()->json(['success' => $insert ? 1 : 0, 'data' => '']);
        }else{
            return response()->json(['success' => 0, 'data' => $validator->errors()->all()]);
        }
    }

    public function postUpdateTurno(Request $request)
    {
        $data = [];
        $trabajador_id = $request->id;
        $sede = $request->d_sede;

        for ($i=0; $i < count($sede); $i++) {
            $data[] = [
                'sede_id' => $request->d_sede[$i],
                'especialidad_id' => $request->d_especialidad[$i],
                'trabajador_id' => $trabajador_id,
                'dia' => $request->d_dia[$i],
                'hora_inicio' => $request->d_desde[$i],
                'hora_fin' => $request->d_hasta[$i]
            ];
        }
        Turno::where('trabajador_id', $trabajador_id)->delete();
        $insert = Turno::insert($data);
        return response()->json([
            'success' => $insert ? 1 : 0
        ]);
    }

    public function getInfo($id = '')
    {
        $detalle = Turno::select('sede_id', 'sede.nombre as sede', 'especialidad_id', 'especialidad.nombre as especialidad', 'dia', 'hora_inicio', 'hora_fin')
            ->join('especialidad','especialidad.id','=','turno.especialidad_id')
            ->join('sede','sede.id','=','turno.sede_id')
            ->where('trabajador_id', $id)
            ->whereNull('turno.fecha_eliminado')
            ->whereNull('especialidad.fecha_eliminado')
            ->orderBy('turno.sede_id')
            ->orderBy('turno.especialidad_id')
            ->orderBy('turno.dia')
            ->orderBy('turno.hora_inicio')
            ->get();
        
        return response()->json([
            'success' => 1,
            'data' => [
                'detalle' => $detalle
            ]
        ]);
    }

    public function getInfoTarifa($id = '')
    {
        $detalle = \DB::select('SELECT es.nombre as especialidad, se.nombre as sede, tu.especialidad_id, tu.sede_id, IFNULL(ta.importe, 0) AS importe FROM turno as tu
        LEFT JOIN tarifario as ta ON (tu.sede_id = ta.sede_id AND tu.especialidad_id = ta.especialidad_id AND tu.trabajador_id = ta.trabajador_id)
        LEFT JOIN especialidad as es ON tu.especialidad_id = es.id
        LEFT JOIN sede as se ON tu.sede_id = se.id
        WHERE tu.trabajador_id = ? 
        GROUP BY especialidad_id, sede_id', [$id]);
        
        return response()->json([
            'success' => 1,
            'data' => [
                'detalle' => $detalle
            ]
        ]);
    }
}
