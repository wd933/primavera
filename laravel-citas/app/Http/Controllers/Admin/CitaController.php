<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cita;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CitaController extends Controller
{
    public function getIndex()
    {
        $data = [];
        $data['estado'] = DB::table('cita_estado')->select(['id', 'nombre'])->whereNull('fecha_eliminado')->orderBy('orden', 'asc')->get();
        return view('cita.index', $data);
    }

    public function getList(Request $request)
    {
        $sql = DB::table('cita as c')
            ->select([
                'c.id',
                DB::raw('concat(p.nombre, \' \', p.apellido_pa, \' \', p.apellido_ma) as paciente'),
                DB::raw('concat(m.nombre, \' \', m.apellido_pa, \' \', m.apellido_ma) as medico'),
                'ce.nombre as estado',
                'c.cita_estado_id',
                DB::raw('fecha_inicio as dia'),
                DB::raw('substr(fecha_inicio, 12, 5) as hora_inicio'),
                DB::raw('substr(fecha_fin, 12, 5) as hora_fin'),
            ])
            ->join('trabajador as m', 'c.trabajador_id', '=', 'm.id')
            ->join('paciente as p', 'c.paciente_id', '=', 'p.id')
            ->join('especialidad as e', 'c.especialidad_id', '=', 'e.id')
            ->join('cita_estado as ce', 'c.cita_estado_id', '=', 'ce.id')
            ->whereNull('c.fecha_eliminado');

        return dataTables()->of($sql)
            ->filter(function ($query) use ($request) {
                if ($request->has('estado') && $request->get('estado') != '') {
                    $query->where('c.cita_estado_id', $request->get('estado'));
                }
            })
            ->editColumn('dia', '{{date(\'d/m/Y\', strtotime($dia))}}')
            ->addColumn('options', '
                <button class="btn btn-check btn-sm btn-success" data-toggle="tooltip" title="Atendida" {{ $cita_estado_id == 1 ? "" : "disabled" }}><i class="fas fa-check"></i></button>
                <button class="btn btn-delete btn-sm btn-danger" {{ $cita_estado_id == 1 ? "" : "disabled" }}><i class="fas fa-trash"></i></button>
            ')
            ->rawColumns(['options'])
            ->make(true);
    }

    public function getInfo($id = '')
    {
        $info = Cita::info($id);
        return response()->json(['success' => ($info ? 1 : 0), 'data' => $info]);
    }

    public function getAction($type = '', $id = '')
    {
        if ($type == 'check') {
            $db = DB::table('cita')->where('id', $id)->update(['cita_estado_id' => 2]);
        }
        if ($type == 'delete') {
            $db = DB::table('cita')->where('id', $id)->update(['cita_estado_id' => 3]);
        }
        return response()->json(['success' => $db]);
    }

    public function postPaciente(Request $request)
    {
        $q = $request->q;
        $sql = DB::table('paciente')
            ->select([
                'id',
                DB::raw('concat(nombre, \' \', apellido_pa, \' \', apellido_ma) as name'),
            ])
            ->where(DB::raw('concat(nombre, \' \', apellido_pa, \' \', apellido_ma)'), 'like', '%' . $q . '%')
            ->whereNull('fecha_eliminado');

        return response()->json(['success' => 1, 'data' => $sql->get()]);
    }

    public function getSede()
    {
        $data = DB::table('sede')
            ->select(['id', 'nombre'])
            ->whereNull('fecha_eliminado')
            ->orderBy('orden', 'asc')
            ->get();

        return response()->json([
            'success' => 1,
            'data' => $data,
        ]);
    }

    public function getEspecialidad($idSede)
    {
        $data = DB::table('especialidad_sede as ss')
            ->select(['es.id', 'es.nombre'])
            ->join('sede as se', 'ss.sede_id', '=', 'se.id')
            ->join('especialidad as es', 'ss.especialidad_id', '=', 'es.id')
            ->where('ss.sede_id', $idSede)
            ->whereNull('ss.fecha_eliminado')
            ->whereNull('se.fecha_eliminado')
            ->whereNull('es.fecha_eliminado')
            ->orderBy('orden', 'asc')
            ->get();

        return response()->json([
            'success' => 1,
            'data' => $data,
        ]);
    }

    public function getMedico($idSede, $idEspecialidad)
    {
        $data = DB::table('turno as tu')
            ->select(['me.id', DB::raw("concat(me.nombre, ' ', me.apellido_pa, ' ', me.apellido_ma) as nombre"), DB::raw('IFNULL(ta.importe, 0) AS importe')])
            ->join('trabajador as me', 'me.id', '=', 'tu.trabajador_id')
            ->leftJoin('tarifario as ta', function ($join) {
                $join->on('ta.cita_motivo_id', '=', DB::raw('1'));
                $join->on('ta.sede_id', '=', 'tu.sede_id');
                $join->on('ta.especialidad_id', '=', 'tu.especialidad_id');
                $join->on('ta.trabajador_id', '=', 'tu.trabajador_id');
            })
            ->where('tu.sede_id', $idSede)
            ->where('tu.especialidad_id', $idEspecialidad)
            ->whereNull('tu.fecha_eliminado')
            ->distinct()
            ->get();

        return response()->json([
            'success' => 1,
            'data' => $data,
        ]);
    }

    public function getMedicoLista()
    {
        $data = DB::table('turno as tu')
            ->select([
                DB::raw("concat(tr.nombre, ' ', tr.apellido_pa, ' ', tr.apellido_ma) as tr_nombre"),
                'es.nombre as es_nombre',
                'se.nombre as se_nombre',
                'ta.sede_id',
                'ta.especialidad_id',
                'ta.trabajador_id',
                DB::raw('IFNULL(ta.importe, 0) AS importe'),
                DB::raw('GROUP_CONCAT(distinct(tu.dia)) dias'),
            ])
            ->join('trabajador as tr', 'tr.id', '=', 'tu.trabajador_id')
            ->join('especialidad as es', 'es.id', '=', 'tu.especialidad_id')
            ->join('sede as se', 'se.id', '=', 'tu.sede_id')
            ->leftJoin('tarifario as ta', function ($join) {
                $join->on('ta.cita_motivo_id', '=', DB::raw('1'));
                $join->on('ta.sede_id', '=', 'tu.sede_id');
                $join->on('ta.especialidad_id', '=', 'tu.especialidad_id');
                $join->on('ta.trabajador_id', '=', 'tu.trabajador_id');
            })
            ->whereNull('tu.fecha_eliminado')
            ->whereNotNull('ta.importe')
            ->groupBy('especialidad_id')
            ->groupBy('trabajador_id')
            ->groupBy('sede_id')
            ->distinct()
            ->get();

        return response()->json([
            'success' => 1,
            'data' => $data,
        ]);
    }

    public function getHora($idSede, $idEspecialidad, $idMedico, $fecha)
    {
        $citas = DB::table('cita')
            ->where('sede_id', $idSede)
            ->where('trabajador_id', $idMedico)
            ->where('especialidad_id', $idEspecialidad)
            ->whereRaw('cita_estado_id != 3')
            ->where(DB::raw('date(fecha_inicio)'), $fecha)
            ->pluck(DB::raw('substr(fecha_inicio,12,5) as hora_inicio'))
            ->toArray();

        $especialidad = DB::table('especialidad as es')
            ->select(['minutos_atencion'])
            ->where('id', $idEspecialidad)
            ->get()
            ->first();

        $intervalo = 10;

        if ($especialidad) {
            $intervalo = $especialidad->minutos_atencion;
        }

        $turnos = DB::table('turno as tu')
            ->select(['hora_inicio', 'hora_fin'])
            ->where('sede_id', $idSede)
            ->where('trabajador_id', $idMedico)
            ->where('especialidad_id', $idEspecialidad)
            ->where('dia', date('N', strtotime($fecha)))
            ->whereNull('tu.fecha_eliminado')
            ->orderBy('hora_inicio', 'asc')
            ->get();

        $data = [];

        foreach ($turnos as $t) {
            $inicio = explode(':', $t->hora_inicio);
            $fin = explode(':', $t->hora_fin);

            $start = $inicio[0] * 60 + $inicio[1];
            $end = $fin[0] * 60 + $fin[1];

            for ($i = $start; $i <= $end; $i = $i + $intervalo) {
                $ini = $fecha . " " . floor($i / 60) . ":" . ($i % 60);
                $fin = $fecha . " " . floor(($i + $intervalo) / 60) . ":" . (($i + $intervalo) % 60);

                $date12 = date('h:iA', strtotime($ini));
                $datei = date('H:i', strtotime($ini));
                $datef = date('H:i', strtotime($fin));
                if (!in_array($datei, $citas) && strtotime($ini) > time()) {
                    $data[] = ['id' => $datei . '-' . $datef, 'nombre' => $date12, 'hora' => $date12, 'inicio' => $datei, 'fin' => $datef];
                }
            }
        }

        return response()->json([
            'success' => 1,
            'data' => $data,
        ]);
    }

    public function postInsert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'paciente_id' => 'required',
            'sede_id' => 'required',
            'especialidad_id' => 'required',
            'medico_id' => 'required',
            'fecha' => 'required',
            'fechab' => 'required',
            'hora' => 'required',
        ]);

        $fecha = implode('-', array_reverse(explode('/', $request->fecha)));
        $hora = explode('-', $request->hora);

        $existe = DB::table('cita')->select(['cita.id'])
            ->where('cita_estado_id', 1)
            ->where('sede_id', $request->sede_id)
            ->where('especialidad_id', $request->especialidad_id)
            ->where('trabajador_id', $request->medico_id)
            ->where('fecha_inicio', $fecha . " " . $hora[0])
            ->get();
        $totalDia = DB::table('cita')->select(['cita.id'])
            ->where('cita_estado_id', 1)
            ->where('sede_id', $request->sede_id)
            ->where('especialidad_id', $request->especialidad_id)
            ->where(DB::raw('date(fecha_inicio)'), $fecha)
            ->get();
        $totalGeneral = DB::table('cita')->select(['cita.id'])
            ->where('cita_estado_id', 1)
            ->where(DB::raw('date(fecha_inicio)'), '>=', date('Y-m-d'))
            ->get();

        if ($validator->fails()) {
            return response()->json(['success' => 0, 'data' => $validator->errors()->all()], 422);
        } else {

            if (count($existe) > 0) {
                return response()->json(['success' => 0, 'data' => ['La fecha y hora no están disponibles']], 422);
            } else if (count($totalDia) > 0) {
                return response()->json(['success' => 0, 'data' => ['Ya cuentas con una cita para el día actual']], 422);
            } else if (count($totalGeneral) > 5) {
                return response()->json(['success' => 0, 'data' => ['Ya cuentas 5 citas activas']], 422);
            } else {
                $insert = DB::table('cita')->insert([
                    'paciente_id' => $request->paciente_id,
                    'sede_id' => $request->sede_id,
                    'especialidad_id' => $request->especialidad_id,
                    'trabajador_id' => $request->medico_id,
                    'cita_estado_id' => 1,
                    'fecha_inicio' => $fecha . " " . $hora[0],
                    'fecha_fin' => $fecha . " " . $hora[1],
                    'importe' => $request->importe,
                ]);

                return response()->json([
                    'success' => $insert ? 1 : 0,
                    'data' => '',
                ]);
            }
        }
    }

    public function listado($idPaciente)
    {
        $citas = DB::table('cita as ci')
            ->select([
                'ci.id',
                DB::raw('date_format(ci.fecha_inicio, \'%d/%m/%Y %h:%i%p\') as fecha'),
                'se.nombre as sede',
                'es.nombre as especialidad',
                DB::raw('concat(pa.nombre, \' \', pa.apellido_pa, \' \', pa.apellido_ma) as paciente'),
                DB::raw('concat(me.nombre, \' \', me.apellido_pa, \' \', me.apellido_ma) as medico'),
                'ce.nombre as estado',
                'ce.color as estado_color',
            ])
            ->join('sede as se', 'ci.sede_id', '=', 'se.id')
            ->join('especialidad as es', 'ci.especialidad_id', '=', 'es.id')
            ->join('paciente as pa', 'ci.paciente_id', '=', 'pa.id')
            ->join('trabajador as me', 'ci.trabajador_id', '=', 'me.id')
            ->join('cita_estado as ce', 'ci.cita_estado_id', '=', 'ce.id')
            ->where('paciente_id', $idPaciente)
            ->orderBy('fecha_inicio', 'desc')
            ->get();

        return response()->json([
            'success' => 1,
            'data' => $citas,
        ]);

    }
}
