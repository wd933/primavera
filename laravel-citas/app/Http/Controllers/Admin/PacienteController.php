<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Identificacion;
use App\Models\Paciente;

class PacienteController extends Controller
{
    public function getIndex()
    {
        $data = [
            'identificacion' => Identificacion::active()->get()
        ];
        return view('paciente.index', $data);
    }

    public function getList()
    {
        $sql = Paciente::listaTabla();

        return dataTables()->of($sql)
            ->addColumn('options', '
                <button class="btn btn-edit btn-sm btn-warning"><i class="fas fa-pencil-alt"></i></button>
                <button class="btn btn-delete btn-sm btn-danger"><i class="fas fa-trash"></i></button>
            ')
            ->rawColumns(['options'])->make(true);
    }

    public function getInfo($id = ''){
        $info = Paciente::info($id);
        return response()->json(['success' => ($info ? 1 : 0), 'data' => $info]);
    }

    public function postInsert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'apellido_pa' => 'required',
            'apellido_ma' => 'required',
            'identificacion_id' => 'required',
            'n_documento' => ['required', Rule::unique('paciente')->where('identificacion_id', $request->identificacion_id ?: '')->whereNull('fecha_eliminado')],
            'direccion' => 'required',
            'celular' => 'nullable|numeric',
            'telefono' => 'nullable|numeric'
        ], [], [
            'apellido_pa' => 'apellido paterno',
            'apellido_ma' => 'apellido materno',
            'n_documento' => 'nro documento',
            'identificacion_id' => 'identificacion'
        ]);
        
        if (!$validator->fails()) {
            $paciente = new Paciente;
            $paciente->nombre = $request->nombre;
            $paciente->apellido_pa = $request->apellido_pa;
            $paciente->apellido_ma = $request->apellido_ma;
            $paciente->identificacion_id = $request->identificacion_id;
            $paciente->n_documento = $request->n_documento;
            $paciente->direccion = $request->direccion;
            $paciente->celular = $request->celular;
            $paciente->telefono = $request->telefono;
            $paciente->fecha_registro = date('Y-m-d H:i');
            $paciente->save();
            return response()->json(['success' => 1, 'data' => '']);
        }else{
            return response()->json(['success' => 0, 'data' => $validator->errors()->all()]);
        }
    }

    public function postUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            'apellido_pa' => 'required',
            'apellido_ma' => 'required',
            'identificacion_id' => 'required',
            'n_documento' => [
                'required',
                Rule::unique('paciente')
                ->where('identificacion_id', $request->identificacion_id ?: '')
                ->whereNull('fecha_eliminado')
            ],
            'direccion' => 'required|min:5',
            'celular' => 'nullable|numeric|digits:9',
            'telefono' => 'nullable|numeric|digits:7'
        ], [], [
            'apellido_pa' => 'apellido paterno',
            'apellido_ma' => 'apellido materno',
            'n_documento' => 'nro documento',
            'identificacion_id' => 'identificacion'
        ]);
        
        if (!$validator->fails()) {
            $paciente = Paciente::active()->find($request->id);
            $paciente->nombre = $request->nombre;
            $paciente->apellido_pa = $request->apellido_pa;
            $paciente->apellido_ma = $request->apellido_ma;
            $paciente->identificacion_id = $request->identificacion_id;
            $paciente->n_documento = $request->n_documento;
            $paciente->direccion = $request->direccion;
            $paciente->celular = $request->celular;
            $paciente->telefono = $request->telefono;
            $paciente->save();
            return response()->json(['success' => 1, 'data' => '']);
        }else{
            return response()->json(['success' => 0, 'data' => $validator->errors()->all()]);
        }
    }
    
    public function postDelete($id)
    {
        $paciente = Paciente::active()->find($id);
        $paciente->fecha_eliminado = date('Y-m-d H:i');
        $paciente->save();
        return response()->json(['success' => 1, 'data' => '']);
    }

    public function registro(Request $request){
        $doc_validation = [];
        $id = Identificacion::find($request->tipo_doc);

        if($id){
            $doc_validation = explode('|', $id->validacion);
        }
        
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'apellido_pa' => 'required',
            'apellido_ma' => 'required',
            'tipo_doc' => 'required',
            'n_documento' => array_merge($doc_validation, [
                'required', 
                Rule::unique('paciente')
                ->where('identificacion_id', $request->tipo_doc ? $request->tipo_doc : '')
                ->whereNull('fecha_eliminado')
            ]),
            'telefono' => '',
            'celular' => 'required|digits:9',
            'fecha_nacimiento' => 'date_format:d/m/Y',
            'sexo' => 'in:1,2',
            //'direccion' => 'required',
            'email' => [
                'required', 
                'email', 
                Rule::unique('users')
                ->whereNull('trabajador_id')
                ->whereNull('deleted_at')
            ],
            'password' => 'required',
            're_password' => 'required'
        ], [], [
            'n_documento' => 'nro. documento'
        ]);

        if ($validator->fails()) {
            return response()->json(['success' => 0, 'data' => $validator->errors()->all()]);
        }else{
            $fecha = date('Y-m-d H:i');
            $fecha_nacimiento = $request->fecha_nacimiento ? implode('-',array_reverse(explode('/',$request->fecha_nacimiento))) : null;
            $sexo = $request->sexo ? ($request->sexo == 1 ? 'M' : 'F') : null;
            $paciente = [
                'nombre' => $request->nombre,
                'apellido_pa' => $request->apellido_pa,
                'apellido_ma' => $request->apellido_ma,
                'identificacion_id' => $request->tipo_doc,
                'n_documento' => $request->n_documento,
                'fecha_registro' => $fecha,
                'fecha_nacimiento' => $fecha_nacimiento,
                'sexo' => $sexo
            ];
            $paciente_id = DB::table('paciente')->insertGetId($paciente);
            
            $user = [
                'paciente_id' => $paciente_id,
                'name' => $request->nombre." ".$request->apellido_pa." ".$request->apellido_ma,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'created_at' => $fecha
            ];
            $id = DB::table('users')->insertGetId($user);

            return response()->json(['success' => 1, 'data' => ['id' => $id, 'nombre' => $user['name'], 'paciente_id' => $paciente_id]]);
        }
    }
}