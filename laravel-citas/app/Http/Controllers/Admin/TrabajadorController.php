<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Models\Cargo;
use App\Models\Identificacion;
use App\Models\Trabajador;

class TrabajadorController extends Controller
{
    public function getIndex()
    {
        $data = [
            'identificacion' => Identificacion::active()->get(),
            'cargo' => Cargo::active()->get()
        ];
        return view('trabajador.index', $data);
    }

    public function getList()
    {
        $sql = Trabajador::listaTabla();

        return dataTables()->of($sql)
            ->addColumn('options', '
                <button class="btn btn-edit btn-sm btn-warning"><i class="fas fa-pencil-alt"></i></button>
                <button class="btn btn-delete btn-sm btn-danger"><i class="fas fa-trash"></i></button>
            ')
            ->rawColumns(['options'])->make(true);
    }

    public function getInfo($id = ''){
        $info = Trabajador::info($id);
        return response()->json(['success' => ($info ? 1 : 0), 'data' => $info]);
    }

    public function postInsert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            'apellido_pa' => 'required',
            'apellido_ma' => 'required',
            'identificacion_id' => 'required',
            'cargo_id' => 'required',
            'n_documento' => [
                'required', 
                Rule::unique('trabajador')->where('identificacion_id', $request->identificacion_id ?: '')->whereNull('fecha_eliminado')
            ],
            'direccion' => 'required|min:5',
            'celular' => 'nullable|numeric|digits:9',
            'telefono' => 'nullable|numeric|digits:7'
        ], [], [
            'apellido_pa' => 'apellido paterno',
            'apellido_ma' => 'apellido materno',
            'n_documento' => 'nro documento',
            'identificacion_id' => 'identificacion',
            'cargo_id' => 'cargo'
        ]);
        
        if (!$validator->fails()) {
            $trabajador = new Trabajador;
            $trabajador->nombre = $request->nombre;
            $trabajador->apellido_pa = $request->apellido_pa;
            $trabajador->apellido_ma = $request->apellido_ma;
            $trabajador->identificacion_id = $request->identificacion_id;
            $trabajador->cargo_id = $request->cargo_id;
            $trabajador->n_documento = $request->n_documento;
            $trabajador->direccion = $request->direccion;
            $trabajador->celular = $request->celular;
            $trabajador->telefono = $request->telefono;
            $trabajador->fecha_registro = date('Y-m-d H:i');
            $trabajador->save();
            return response()->json(['success' => 1, 'data' => '']);
        }else{
            return response()->json(['success' => 0, 'data' => $validator->errors()->all()]);
        }
    }

    public function postUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            'apellido_pa' => 'required',
            'apellido_ma' => 'required',
            'identificacion_id' => 'required',
            'cargo_id' => 'required',
            'n_documento' => [
                'required', 
                Rule::unique('trabajador')->where('id', '<>', $request->id)->where('identificacion_id', $request->identificacion_id ?: '')->whereNull('fecha_eliminado')
            ],
            'direccion' => 'required|min:5',
            'celular' => 'nullable|numeric|digits:9',
            'telefono' => 'nullable|numeric|digits:7'
        ], [], [
            'apellido_pa' => 'apellido paterno',
            'apellido_ma' => 'apellido materno',
            'n_documento' => 'nro documento',
            'identificacion_id' => 'identificacion',
            'cargo_id' => 'cargo'
        ]);
        
        if (!$validator->fails()) {
            $trabajador = Trabajador::active()->find($request->id);
            $trabajador->nombre = $request->nombre;
            $trabajador->apellido_pa = $request->apellido_pa;
            $trabajador->apellido_ma = $request->apellido_ma;
            $trabajador->identificacion_id = $request->identificacion_id;
            $trabajador->cargo_id = $request->cargo_id;
            $trabajador->n_documento = $request->n_documento;
            $trabajador->direccion = $request->direccion;
            $trabajador->celular = $request->celular;
            $trabajador->telefono = $request->telefono;
            $trabajador->save();
            return response()->json(['success' => 1, 'data' => '']);
        }else{
            return response()->json(['success' => 0, 'data' => $validator->errors()->all()]);
        }
    }
    
    public function postDelete($id)
    {
        $trabajador = Trabajador::active()->find($id);
        $trabajador->fecha_eliminado = date('Y-m-d H:i');
        $trabajador->save();
        return response()->json(['success' => 1, 'data' => '']);
    }

}
