<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Especialidad;

class EspecialidadController extends Controller
{
    public function getIndex()
    {
        return view('especialidad.index');
    }

    public function getList()
    {
        $sql = Especialidad::listaTabla();

        return dataTables()->of($sql)
            ->addColumn('options', '
                <button class="btn btn-edit btn-sm btn-warning"><i class="fas fa-pencil-alt"></i></button>
                <button class="btn btn-delete btn-sm btn-danger"><i class="fas fa-trash"></i></button>
            ')
            ->rawColumns(['options'])->make(true);
    }

    public function getInfo($id = ''){
        $info = Especialidad::info($id);
        return response()->json(['success' => ($info ? 1 : 0), 'data' => $info]);
    }

    public function postInsert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'descripcion' => '',
            'minutos_atencion' => 'required|numeric'
        ]);
        
        if (!$validator->fails()) {
            $especialidad = new Especialidad;
            $especialidad->nombre = $request->nombre;
            $especialidad->descripcion = $request->descripcion;
            $especialidad->minutos_atencion = $request->minutos_atencion;
            $especialidad->save();
            return response()->json(['success' => 1, 'data' => '']);
        }else{
            return response()->json(['success' => 0, 'data' => $validator->errors()->all()]);
        }
    }

    public function postUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            'nombre' => 'required',
            'descripcion' => '',
            'minutos_atencion' => 'required|numeric'
        ]);
        
        if (!$validator->fails()) {
            $especialidad = Especialidad::active()->find($request->id);
            $especialidad->nombre = $request->nombre;
            $especialidad->descripcion = $request->descripcion;
            $especialidad->minutos_atencion = $request->minutos_atencion;
            $especialidad->save();
            return response()->json(['success' => 1, 'data' => '']);
        }else{
            return response()->json(['success' => 0, 'data' => $validator->errors()->all()]);
        }
    }
    
    public function postDelete($id)
    {
        $especialidad = Especialidad::active()->find($id);
        $especialidad->fecha_eliminado = date('Y-m-d H:i');
        $especialidad->save();
        return response()->json(['success' => 1, 'data' => '']);
    }

}
