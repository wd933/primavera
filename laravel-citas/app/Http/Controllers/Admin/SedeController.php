<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\Models\Sede;

class SedeController extends Controller
{
    public function getIndex()
    {
        return view('sede.index');
    }

    public function getList()
    {
        $sql = Sede::listaTabla();

        return dataTables()->of($sql)
            ->addColumn('options', '
                <button class="btn btn-edit btn-sm btn-warning"><i class="fas fa-pencil-alt"></i></button>
                <button class="btn btn-delete btn-sm btn-danger"><i class="fas fa-trash"></i></button>
            ')
            ->rawColumns(['options'])->make(true);
    }

    public function getInfo($id = ''){
        $info = Sede::info($id);
        return response()->json(['success' => ($info ? 1 : 0), 'data' => $info]);
    }

    public function postInsert(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre' => 'required',
            'direccion' => 'required',
            'telefono' => 'required',
            'email' => 'nullable|email',
            'google_maps' => 'nullable|url',
        ]);
        
        if (!$validator->fails()) {
            $sede = new Sede;
            $sede->nombre = $request->nombre;
            $sede->direccion = $request->direccion;
            $sede->telefono = $request->telefono;
            $sede->email = $request->email;
            $sede->google_maps = $request->google_maps;
            $sede->save();
            return response()->json(['success' => 1, 'data' => '']);
        }else{
            return response()->json(['success' => 0, 'data' => $validator->errors()->all()]);
        }
    }

    public function postUpdate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|numeric',
            'nombre' => 'required',
            'direccion' => 'required',
            'telefono' => 'required',
            'email' => 'nullable|email',
            'google_maps' => 'nullable|url',
        ]);
        
        if (!$validator->fails()) {
            $sede = Sede::active()->find($request->id);
            $sede->nombre = $request->nombre;
            $sede->direccion = $request->direccion;
            $sede->telefono = $request->telefono;
            $sede->email = $request->email;
            $sede->google_maps = $request->google_maps;
            $sede->save();
            return response()->json(['success' => 1, 'data' => '']);
        }else{
            return response()->json(['success' => 0, 'data' => $validator->errors()->all()]);
        }
    }
    
    public function postDelete($id)
    {
        $sede = Sede::active()->find($id);
        $sede->fecha_eliminado = date('Y-m-d H:i');
        $sede->save();
        return response()->json(['success' => 1, 'data' => '']);
    }

}
