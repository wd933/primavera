<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MedicoController extends Controller
{
    public function getIndex()
    {
        return view('medico.index');
    }

    public function getList()
    {
        $sql = DB::table('medico as m')
            ->select(['m.id','m.nombre','m.apellido_pa','m.apellido_ma','m.n_documento','m.suspendido','e.nombre as especialidad'])
            ->join('especialidad as e', 'm.especialidad_id', '=', 'e.id')
            ->whereNull('m.fecha_eliminado');
        
        $sql = DB::table( DB::raw("({$sql->toSql()}) as sub") );

        return dataTables()->of($sql)
            ->addColumn('options', '
                <button class="btn btn-edit btn-sm btn-warning"><i class="fas fa-pencil-alt"></i></button>
                <button class="btn btn-delete btn-sm btn-danger"><i class="fas fa-trash"></i></button>
            ')
            ->rawColumns(['options'])
            ->make(true);
    }

    public function getForm($id = '')
    {
        $identificacion = DB::table('identificacion')->select(['id', 'nombre'])->get();

        return response()->json([
            'success' => 1,
            'data' => [
                'identificacion' => $identificacion
            ]
        ]);
    }

    public function listado(){
        $medicos = DB::table('turno as t')
            ->select([
                DB::raw('concat(me.nombre, \' \', me.apellido_pa, \' \', me.apellido_ma) as medico'),
                DB::raw('GROUP_CONCAT(DISTINCT es.nombre) as especialidad'),
                DB::raw('GROUP_CONCAT(DISTINCT se.nombre) as sede'),
            ])
            ->join('especialidad as es', 't.especialidad_id', '=', 'es.id')
            ->join('sede as se', 't.sede_id', '=', 'se.id')
            ->join('trabajador as me', 't.trabajador_id', '=', 'me.id')
            ->whereNull('me.fecha_eliminado')
            ->groupBy('trabajador_id')
            ->get();
            
        return response()->json([
            'success' => 1,
            'data' => $medicos
        ]);

    }
}
