<?php

namespace App\Http\Controllers\Api;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{

    public function loginUser(Request $request)
    {
        $email           = $request->email;
        $password      = $request->password;
        $rememberToken = $request->remember;

        if (Auth::guard('web')->attempt(['email' => $email, 'password' => $password], $rememberToken)) {
            return response()->json([
                'success'  => 1,
                'message' => 'Login Successful',
                'data' => [
                    'id' => Auth::user()->id,
                    'nombre' => Auth::user()->name,
                    'paciente_id' => Auth::user()->paciente_id
                ]
            ]);
        } else {
            return response()->json([
                'success'  => 0,
                'message' => 'Login Fail !'
            ]);
        }
    }
}
