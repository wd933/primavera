<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;

class VencimientoCita extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:vencimiento-cita';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $ayer = date('Y-m-d', strtotime('yesterday'));
        // echo $ayer;
        DB::update('UPDATE cita SET cita_estado_id = 4 WHERE cita_estado_id = 1 AND fecha_inicio <= ?', [$ayer]);
    }
}
