<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    public $timestamps = false;
    protected $table = 'paciente';

    public function scopeActive($query)
    {
        return $query->whereNull('fecha_eliminado');
    }

    public static function listaTabla()
    {
        $sql = DB::table('paciente')
            ->select(['id','nombre','apellido_pa','apellido_ma','n_documento','suspendido'])
            ->whereNull('fecha_eliminado');
                    
        return DB::table( DB::raw("({$sql->toSql()}) as sub") );
    }

    public static function info($id)
    {
        return DB::table('paciente')
            ->select(['id','nombre','apellido_pa','apellido_ma','identificacion_id','n_documento','direccion','celular','telefono'])
            ->whereNull('fecha_eliminado')
            ->where('id', $id)
            ->get()
            ->first();
    }

}
