<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Trabajador extends Model
{
    public $timestamps = false;
    protected $table = 'trabajador';

    public function scopeActive($query)
    {
        return $query->whereNull('fecha_eliminado');
    }

    public static function listaTabla($idCargo = '')
    {
        $sql = DB::table('trabajador as t')
            ->select(['t.id','t.nombre','t.apellido_pa','t.apellido_ma','t.n_documento','t.suspendido','c.nombre as cargo'])
            ->join('cargo as c', 't.cargo_id', '=', 'c.id')
            ->whereNull('t.fecha_eliminado')
            ->when($idCargo != '', function ($q) use ($idCargo) {
                //return $q->where('t.cargo_id', '=', $idCargo);
                return $q->whereRaw('t.cargo_id = '.$idCargo);
            });
            
        return DB::table( DB::raw("({$sql->toSql()}) as sub") );
    }

    public static function info($id)
    {
        return DB::table('trabajador')
            ->select(['id','nombre','apellido_pa','apellido_ma','identificacion_id','cargo_id','n_documento','direccion','celular','telefono'])
            ->whereNull('fecha_eliminado')
            ->where('id', $id)
            ->get()
            ->first();
    }

}
