<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Especialidad extends Model
{
    public $timestamps = false;
    protected $table = 'especialidad';

    public function scopeActive($query)
    {
        return $query->whereNull('fecha_eliminado');
    }

    public static function listaTabla()
    {
        return DB::table('especialidad')
            ->select(['id','nombre','descripcion','minutos_atencion'])
            ->whereNull('fecha_eliminado');
    }

    public static function info($id)
    {
        return DB::table('especialidad')
            ->select(['id','nombre','descripcion','minutos_atencion'])
            ->whereNull('fecha_eliminado')
            ->where('id', $id)
            ->get()
            ->first();
    }

}
