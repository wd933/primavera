<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Tarifario extends Model
{
    public $timestamps = false;
    protected $table = 'tarifario';
    protected $fillable = ['importe'];

    public function scopeActive($query)
    {
        return $query->whereNull('fecha_eliminado');
    }

}
