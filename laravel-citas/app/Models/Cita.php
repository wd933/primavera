<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    public $timestamps = false;
    protected $table = 'especialidad';

    public function scopeActive($query)
    {
        return $query->whereNull('fecha_eliminado');
    }

    public static function listaTabla()
    {
        //
    }

    public static function info($id)
    {
        return DB::table('cita as c')
            ->select([
                'c.id', 
                'c.sede_id',
                'c.especialidad_id',
                'c.paciente_id',
                'c.trabajador_id',
                'c.cita_estado_id',
                'c.fecha_inicio',
                'c.fecha_fin',
                'c.fecha_eliminado',
                'se.nombre as sede',
                'es.nombre as especialidad',
                'ce.nombre as estado',
                DB::raw('date_format(c.fecha_inicio, \'%d/%m/%Y\') as fecha_cita'),
                DB::raw('date_format(c.fecha_inicio, \'%h:%i%p\') as cita_inicio'),
                DB::raw('date_format(c.fecha_fin, \'%h:%i%p\') as cita_fin'),
                DB::raw("concat(pa.nombre, ' ', pa.apellido_pa, ' ', pa.apellido_ma) as paciente"),
                DB::raw("concat(tr.nombre, ' ', tr.apellido_pa, ' ', tr.apellido_ma) as medico")
            ])
            ->join('sede as se', 'c.sede_id', '=', 'se.id')
            ->join('especialidad as es', 'c.especialidad_id', '=', 'es.id')
            ->join('paciente as pa', 'c.paciente_id', '=', 'pa.id')
            ->join('trabajador as tr', 'c.trabajador_id', '=', 'tr.id')
            ->join('cita_estado as ce', 'c.cita_estado_id', '=', 'ce.id')
            ->where('c.id', $id)
            ->whereNull('c.fecha_eliminado')
            ->get()
            ->first();
    }

}
