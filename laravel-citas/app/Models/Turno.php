<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Turno extends Model
{
    public $timestamps = false;
    protected $table = 'turno';

    public function scopeActive($query)
    {
        return $query->whereNull('fecha_eliminado');
    }

    public function sede()
    {
        return $this->belongsTo('App\Models\Sede')->withDefault();
    }

    public function especialidad()
    {
        return $this->belongsTo('App\Models\Especialidad')->withDefault();
    }

    public function trabajador()
    {
        return $this->belongsTo('App\Models\Trabajador')->withDefault();
    }

}
