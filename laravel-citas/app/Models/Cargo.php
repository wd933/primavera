<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    public $timestamps = false;
    protected $table = 'cargo';

    public function scopeActive($query)
    {
        return $query->whereNull('fecha_eliminado');
    }

}