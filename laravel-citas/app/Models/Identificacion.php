<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Identificacion extends Model
{
    public $timestamps = false;
    protected $table = 'identificacion';

    public function scopeActive($query)
    {
        return $query->whereNull('fecha_eliminado');
    }

}
