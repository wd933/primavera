<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Sede extends Model
{
    public $timestamps = false;
    protected $table = 'sede';

    public function scopeActive($query)
    {
        return $query->whereNull('fecha_eliminado');
    }

    public static function listaTabla()
    {
        return DB::table('sede')
            ->select(['id','nombre','direccion','telefono','email'])
            ->whereNull('fecha_eliminado');
    }

    public static function info($id)
    {
        return DB::table('sede')
            ->select(['id','nombre','direccion','telefono','email','google_maps'])
            ->whereNull('fecha_eliminado')
            ->where('id', $id)
            ->get()
            ->first();
    }

}
