import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Device } from '@ionic-native/device/ngx';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';
import { Network } from '@ionic-native/network/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { FullComponent } from './layout/full/full.component';
import { PageComponent } from './layout/page/page.component';
import { SubmenuComponent } from './layout/submenu/submenu.component';

import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { SedeComponent } from './components/sede/sede.component';
import { MedicoComponent } from './components/medico/medico.component';
import { EspecialidadComponent } from './components/especialidad/especialidad.component';
import { CitaComponent } from './components/cita/cita.component';
import { CitaDetalleComponent } from './components/cita-detalle/cita-detalle.component';
import { CitaRegistroComponent } from './components/cita-registro/cita-registro.component';

import { HttpClientModule } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { AppMaterialModule } from './material-module';
import { DatePicker } from '@ionic-native/date-picker/ngx';

import {
  MAT_MOMENT_DATE_FORMATS,
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from '@angular/material-moment-adapter';

import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import {AppMaterialModule} from './material-module';

@NgModule({
  declarations: [
    AppComponent,
    FullComponent,
    PageComponent,
    SubmenuComponent,
    HomeComponent,
    SedeComponent,
    MedicoComponent,
    EspecialidadComponent,
    CitaComponent,
    CitaDetalleComponent,
    CitaRegistroComponent,
    LoginComponent,
    RegisterComponent
  ],
  entryComponents: [],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    IonicModule.forRoot({animated: false}),
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppMaterialModule
    //AppMaterialModule
  ],
  providers: [
    DatePicker,
    StatusBar,  
    SplashScreen,
    AppMinimize,
    Device,
    SpinnerDialog,
    Network,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    // Start DatePicker
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    // End DatePicker
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
