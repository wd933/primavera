import { Component, OnDestroy, AfterViewInit } from '@angular/core';

import { Device } from '@ionic-native/device/ngx';
import { AppMinimize } from '@ionic-native/app-minimize/ngx';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnDestroy, AfterViewInit {
  backButtonSubscription;

  navLinksArray = [];// store route links as the user navigates the app

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private router: Router,
    private device: Device,
    private appMinimize: AppMinimize,
    private splashScreen: SplashScreen
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      //this.statusBar.styleDefault();
      this.statusBar.styleLightContent();
      // let status bar overlay webview
      //this.statusBar.overlaysWebView(true);
      // set status bar to white
      this.statusBar.backgroundColorByHexString('#00255d');
      this.splashScreen.hide();
    });

    //Start Go back
    /*
    this.router.events.subscribe(event => {
      const url = this.router.url //current url
      if (event instanceof NavigationEnd) {
        const isCurrentUrlSaved = this.navLinksArray.find((item) => {return (item === url);});
        if (!isCurrentUrlSaved) this.navLinksArray.push(url);
      }// end event if stmt
    }) // end subscribe
    this.hardwareBackButton();
    */
    // End Go back

  }
/*
  hardwareBackButton() {
    this.platform.backButton.subscribe(() => {
      if (this.navLinksArray.length > 1) {
        this.navLinksArray.pop();
        const index = this.navLinksArray.length + 1;
        const url = this.navLinksArray[index];
        this.router.navigate([url]);
      }
    }) // end subscription
  } // end back button fn
  */

 ngAfterViewInit() {
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      console.log('Device: '+this.device.platform);
      if(this.router.url === '/' || this.router.url === '/home'){
        console.log('salir');
        if (this.device.platform.toLowerCase() == "android") {
          console.log('minimize');
          this.appMinimize.minimize();
        }else{
          console.log('exitApp');
          navigator['app'].exitApp();
        }
      }else {
        console.log('no salir');
      }
    });
  }

  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }
}
