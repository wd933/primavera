import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router) {}

  setLogin(session) {
    let horas = 365 * 24;
    let time = Math.floor(new Date().getTime() / 1000) + horas * 60 * 60;
    localStorage.setItem('expire', time.toString());
    localStorage.setItem('session', JSON.stringify(session));
  }

  activeLogin() {
    let timeSec = Math.floor(new Date().getTime() / 1000);
    let expire = parseInt(localStorage.getItem('expire')) || 0;
    if(expire == 0 || expire < timeSec) {
      return false;
    } else {
      return true;
    }
  }

  user() {
    return JSON.parse(localStorage.getItem('session'));
  }

  userName() {
    return this.user()['nombre'];
  }

  userId() {
    return this.user()['id'];
  }

  logout() {
    localStorage.removeItem('expire');
    localStorage.removeItem('session');
  }
  
}
