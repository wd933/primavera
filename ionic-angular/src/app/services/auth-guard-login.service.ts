import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { NavController } from '@ionic/angular';

import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardLoginService implements CanActivate {

  constructor(
    public router: Router,
    private navCtrl: NavController,
    private authService: AuthService
  ) { }

  canActivate(): boolean {
    if (this.authService.activeLogin()) {
      this.router.navigate(['/home']);
      return false;
    }
    return true;
  }
}
