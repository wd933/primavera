import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { NetworkService, ConnectionStatus } from './network.service';
import { environment } from '../../environments/environment';

import { ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  headers: {};

  constructor(
    public http: HttpClient,
    private toastController: ToastController,
    private alertController: AlertController,
    private networkService: NetworkService,
  ) {
    this.headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Authorization': environment.apiAuth
    };
  }

  async handleError(error: any) {
    var disconnect = false;
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline) {
      disconnect = true;
    }

    console.log(error);
    // poner errores
var message =  error.status + ' ' + error.statusText + ' / ' + (disconnect ? 'Sin conexión' : 'Con conex')
    const alert = await this.alertController.create({
      // header: 'Alerta',
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  get(url) {
    const headers = new HttpHeaders(this.headers);
    return this.http.get(environment.apiUrl + 'asd' + url, { headers });
  }

  post(url, data) {
    const headers = new HttpHeaders(this.headers);
    return this.http.post(environment.apiUrl + url, data, { headers });
  }

  put(url, data) {
    const headers = new HttpHeaders(this.headers);
    return this.http.put(environment.apiUrl + url, data, { headers });
  }

  delete(url) {
    const headers = new HttpHeaders(this.headers);
    return this.http.delete(environment.apiUrl + url, { headers });
  }
}