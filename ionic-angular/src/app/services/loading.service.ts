import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  isLoading = false;

  constructor(
    public loadingController: LoadingController,
    // private spinnerDialog: SpinnerDialog
  ) { }

  async present() {
    this.isLoading = true;
    return await this.loadingController.create({
      duration: 25000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  // async present() {
    // this.spinnerDialog.show();
  // }

  // async dismiss() {
    // this.spinnerDialog.hide();
  // }
}
