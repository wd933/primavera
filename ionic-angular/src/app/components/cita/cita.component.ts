import { Component, OnInit } from '@angular/core';

import { LoadingService } from './../../services/loading.service';
import { ApiService } from './../../services/api.service';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-cita',
  templateUrl: './cita.component.html',
  styleUrls: ['./cita.component.scss'],
})
export class CitaComponent implements OnInit {

  public loading = true;
  public refresh = false;
  public listado = [];

  constructor(
    private apiService: ApiService,
    private authService: AuthService,
  ) {
  }

  ngOnInit() {
    // this.cargarLista();
  }

  ionViewWillEnter() {
    this.listado = [];
    this.cargarLista();
  }

  cargarLista(refresh = false, refresher?) {
    this.refresh = refresh;
    this.loading = true;
    this.apiService.get('/cita/listado/' + this.authService.user()['paciente_id']).subscribe(
      data => (this.listado = data['data']),
      error => this.apiService.handleError(error)
    ).add(() => {
      this.loading = false;
      if (refresher) {
        refresher.target.complete();
      }
    });
  }

}
