import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

import { ApiService } from '../../services/api.service';
import { AuthService } from '../../services/auth.service';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  constructor(
    public loadingService: LoadingService,
    public apiService: ApiService,
    public authService: AuthService,
    public toastController: ToastController,
    public alertController: AlertController,
    public router: Router
  ) { }

  ngOnInit() {
    //this.buscar();
  }

  buscar() {
    this.loadingService.present();
    this.apiService.get('/test')
      .subscribe(
        async data => {
          this.loadingService.dismiss();
          let result = data["data"];
          alert(result);
          if (result.length == 0) {
            const toast = await this.toastController.create({
              message: 'No hay resultados.',
              duration: 2000
            });
            toast.present();
          }
        },
        error => {
          this.loadingService.dismiss();
          console.log('oops', error);
        }
      );
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/']);
    //window.history.go(-(window.history.length-1));
  }
}
