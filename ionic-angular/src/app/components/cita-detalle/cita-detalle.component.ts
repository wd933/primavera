import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

import { LoadingService } from './../../services/loading.service';
import { ApiService } from './../../services/api.service';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-cita-detalle',
  templateUrl: './cita-detalle.component.html',
  styleUrls: ['./cita-detalle.component.scss'],
})
export class CitaDetalleComponent implements OnInit {

  public refresh = false;
  public loading = true;

  public id: string = '';
  public paciente: string = '';
  public medico: string = '';
  public especialidad: string = '';
  public fecha: string = '';
  public hora: string = '';
  public sede: string = '';
  public cita_estado_id: number = 0;

  constructor(
    private route: ActivatedRoute,
    private alertController: AlertController,
    private toastController: ToastController,
    private loadingService: LoadingService,
    private apiService: ApiService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.cargarDatos();
  }

  back() {
    history.back();
  }

  cargarDatos(refresh = false, refresher?) {
    this.refresh = refresh;
    this.loading = true;
    this.apiService.get('/cita/' + this.id).subscribe(
      data => {
        this.sede = data['data']['sede'];
        this.especialidad = data['data']['especialidad'];
        this.fecha = data['data']['fecha_cita'];
        this.hora = data['data']['cita_inicio'];
        this.paciente = data['data']['paciente'];
        this.medico = data['data']['medico'];
        this.cita_estado_id = data['data']['cita_estado_id'];
      },
      error => this.apiService.handleError(error)
    ).add(() => {
      this.loading = false;
      if (refresher) {
        refresher.target.complete();
      }
    });
  }

  async anular() {
    const alert = await this.alertController.create({
      header: 'Importante',
      message: '¿Desea anular la cita?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Anular',
          handler: () => {
            this.loading = true;
            this.apiService.get('/cita/' + this.id).subscribe(
              data => {
                window.history.back();
              },
              error => this.apiService.handleError(error)
            ).add(() => {
              this.loading = false;
            });
          }
        }
      ]
    });

    await alert.present();
  }

}
