import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';

import { LoadingService } from './../../services/loading.service';
import { ApiService } from './../../services/api.service';
import { AuthService } from './../../services/auth.service';

import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
  selector: 'app-cita-registro',
  templateUrl: './cita-registro.component.html',
  styleUrls: ['./cita-registro.component.scss'],
})
export class CitaRegistroComponent implements OnInit {

  public loading = false;
  public id: string;
  public step: number;
  public completado: number = 0;

  public listaUnica: any[] = [];

  public listaSede: any[] = [];
  public listaEspecialidad: any[] = [];
  public listaFecha: any[] = [];
  public listaMedico: any[] = [];
  public listaHoras: any[] = [];

  public idSede: number = null;
  public idEspecialidad: number = null;
  public idMedico: number = null;
  public horaInicio: string = null;
  public horaFin: string = null;
  public importe: number = 0;
  public diasDisponibles: string = '';

  public fecha: string = null;

  private result: any[];

  constructor(
    private toastController: ToastController,
    private loadingService: LoadingService,
    private apiService: ApiService,
    private authService: AuthService
  ) {
    this.cargarLista();
  }

  ngOnInit() {
  }

  back() {
    history.back();
  }

  cargarLista() {
    this.loading = true;
    this.apiService.get('/cita/medico-lista')
      .subscribe(
        async data => {
          this.loading = false;
          if (data['success'] == 1) {
            this.listaUnica = data['data'];
            this.cargarSede();
          } else {
            const toast = await this.toastController.create({
              message: 'Ha ocurrido un error.',
              duration: 2000
            });
            toast.present();
          }
        },
        error => {
          this.loading = false;
          console.log('oops', error);
        }
      );
  }

  cargarSede() {
    for (let i = 0; i < this.listaUnica.length; i++) {
      const e = this.listaUnica[i];
      this.listaSede.push({ id: e.sede_id, nombre: e.se_nombre });
    }
    this.listaSede = this.limpiarOrdenar(this.listaSede);
  }

  cargarEspecialidad() {
    this.listaEspecialidad = [];
    this.listaMedico = [];
    this.listaFecha = [];
    this.idEspecialidad = null;
    this.idMedico = null;
    this.fecha = null;
    console.log('Carga especialidad');
    for (let i = 0; i < this.listaUnica.length; i++) {
      const e = this.listaUnica[i];
      if (this.idSede == e.sede_id) {
        this.listaEspecialidad.push({ id: e.especialidad_id, nombre: e.es_nombre });
        console.log(e);
      }
    }
    this.listaEspecialidad = this.limpiarOrdenar(this.listaEspecialidad);
  }

  cargarMedico() {
    this.listaMedico = [];
    this.listaFecha = [];
    this.idMedico = null;
    this.fecha = null;
    console.log('Carga medico');
    for (let i = 0; i < this.listaUnica.length; i++) {
      const e = this.listaUnica[i];
      if (this.idEspecialidad == e.especialidad_id && this.idSede == e.sede_id) {
        this.listaMedico.push({ id: e.trabajador_id, nombre: e.tr_nombre, importe: e.importe, dias: e.dias });
        console.log(e);
      }
    }
    this.listaMedico = this.limpiarOrdenar(this.listaMedico);
  }

  limpiarOrdenar(array) {
    return (_.sortBy(_.uniqBy(array, 'id'), ['nombre']));
  }

  cargarHora() {
    this.listaHoras = [];
    this.setHora(null, null);

    this.loadingService.present();
    var f = moment(this.fecha).format('YYYY-MM-DD');
    this.apiService.get('/cita/horario/' + this.idSede + '/' + this.idEspecialidad + '/' + this.idMedico + '/' + f)
      .subscribe(
        async data => {
          this.loadingService.dismiss();
          if (data['success'] == 1) {
            this.listaHoras = data['data'];
          } else {
            const toast = await this.toastController.create({
              message: 'Datos incorrectos.',
              duration: 2000
            });
            toast.present();
          }
        },
        error => {
          this.loadingService.dismiss();
          console.log('oops', error);
        }
      );
  }

  cargarFecha() {
    this.listaFecha = [];
    this.fecha = null;

    this.horaInicio = null;
    this.horaFin = null;
    for (let i = 0; i < this.listaMedico.length; i++) {
      if (this.listaMedico[i].id == this.idMedico) {
        this.importe = parseFloat(this.listaMedico[i].importe);
        this.diasDisponibles = this.listaMedico[i].dias;
      }
    }
    moment.locale('es');
    for (let i = 0; i < 15; i++) {
      var f = moment().add(i, 'days');
      var id = f.format('YYYY-MM-DD');
      var fecha = f.format('dddd, DD/MM/YYYY');
      var dia = parseInt(f.format('e')) + 1;
      if (this.diasDisponibles.indexOf(dia.toString()) >= 0) {
        this.listaFecha.push({ id: id, nombre: fecha, dia: dia });
      }
    }
    console.log(this.listaFecha);
  }

  setHora(horaInicio, horaFin) {
    this.horaInicio = horaInicio;
    this.horaFin = horaFin;
  }

  submitCita() {
    var f = moment(this.fecha).format('DD/MM/YYYY');
    var data = {
      paciente_id: this.authService.user()['paciente_id'],
      sede_id: this.idSede,
      especialidad_id: this.idEspecialidad,
      medico_id: this.idMedico,
      hora: this.horaInicio + '-' + this.horaFin,
      importe: this.importe,
      fecha: f
    };
    
    this.loadingService.present();
    this.apiService.post('/cita/insert', data).subscribe(
      data => (this.completado = 1),
      error => this.apiService.handleError(error)
    ).add(() => {
      this.loadingService.dismiss();
    });

    // this.apiService.post('/cita/insert', data)
    //   .subscribe(
    //     async data => {
    //       this.loadingService.dismiss();
    //       if (data['success'] == 1) {
    //         /*
    //         const toast = await this.toastController.create({
    //           message: 'Datos ingresados.',
    //           duration: 2000
    //         });
    //         toast.present();
    //         */
    //         this.completado = 1;
    //       } else {
    //         const toast = await this.toastController.create({
    //           message: data['data'].join('<br>'),
    //           duration: 2000
    //         });
    //         toast.present();
    //       }
    //     },
    //     error => {
    //       console.log('oops', error);
    //     }
    //   );
  }

  get textPaciente() {
    return this.authService.userName();
  }

  get textMedico() {
    var text = '';
    for (let i = 0; i < this.listaMedico.length; i++) {
      if (this.idMedico == this.listaMedico[i].id) {
        text = this.listaMedico[i].nombre;
      }
    }
    return text;
  }

  get textSede() {
    var text = '';
    for (let i = 0; i < this.listaSede.length; i++) {
      if (this.idSede == this.listaSede[i].id) {
        text = this.listaSede[i].nombre;
      }
    }
    return text;
  }

  get textEspecialidad() {
    var text = '';
    for (let i = 0; i < this.listaEspecialidad.length; i++) {
      if (this.idEspecialidad == this.listaEspecialidad[i].id) {
        text = this.listaEspecialidad[i].nombre;
      }
    }
    return text;
  }

  get textFecha() {
    var text = '';
    for (let i = 0; i < this.listaHoras.length; i++) {
      if (this.horaInicio == this.listaHoras[i].inicio) {
        text = this.listaHoras[i].hora;
      }
    }
    return moment(this.fecha).format('DD/MM/YYYY') + ' ' + text;
  }
}