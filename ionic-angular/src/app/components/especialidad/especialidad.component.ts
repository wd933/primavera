import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-especialidad',
  templateUrl: './especialidad.component.html',
  styleUrls: ['./especialidad.component.scss'],
})
export class EspecialidadComponent implements OnInit {

  public listado = [
    {especialidad:'Medicina General'},
    {especialidad:'Pediatría'},
    {especialidad:'Obstetricía'},
    {especialidad:'Odontología'},
  ]
  constructor() { }

  ngOnInit() {
  }

}
