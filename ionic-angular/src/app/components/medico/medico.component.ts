import { Component, OnInit } from '@angular/core';

import { LoadingService } from './../../services/loading.service';
import { ApiService } from './../../services/api.service';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.scss'],
})
export class MedicoComponent implements OnInit {

  public loading = true;
  public refresh = false;
  public listado = [];

  constructor(
    private loadingService: LoadingService,
    private apiService: ApiService,
  ) {
    this.cargarLista();
  }

  ngOnInit() {
  }

  cargarLista(refresh = false, refresher?) {
    this.refresh = refresh;
    this.loading = true;
    this.apiService.get('/medico/listado').subscribe(
      data => (this.listado = data['data']),
      error => this.apiService.handleError(error)
    ).add(() => {
      this.loading = false;
      if (refresher) {
        refresher.target.complete();
      }
    });
  }
}
