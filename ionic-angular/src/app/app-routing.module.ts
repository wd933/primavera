import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FullComponent } from './layout/full/full.component';
import { PageComponent } from './layout/page/page.component';
import { SubmenuComponent } from './layout/submenu/submenu.component';

import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { SedeComponent } from './components/sede/sede.component';
import { MedicoComponent } from './components/medico/medico.component';
import { EspecialidadComponent } from './components/especialidad/especialidad.component';
import { CitaComponent } from './components/cita/cita.component';
import { CitaDetalleComponent } from './components/cita-detalle/cita-detalle.component';
import { CitaRegistroComponent } from './components/cita-registro/cita-registro.component';

import { AuthGuardLoginService as GuardLogin } from './services/auth-guard-login.service';
import { AuthGuardPagesService as GuardPages } from './services/auth-guard-pages.service';

const routes: Routes = [
  {
    path: '',
    component: FullComponent,
    children: [
      { path: '', component: LoginComponent, canActivate: [GuardLogin]  },
      { path: 'register', component: RegisterComponent },
      // { path: 'forgot', loadChildren: './auth/forgot/forgot.module#ForgotPageModule' },
      // { path: 'register', loadChildren: './auth/register/register.module#RegisterPageModule' },
    ]
  },
  {
    path: '',
    component: PageComponent,
    children: [
      { path: 'home', component: HomeComponent },
      { path: 'sede', component: SedeComponent },
      { path: 'medico', component: MedicoComponent },
      { path: 'especialidad', component: EspecialidadComponent },
      { path: 'cita', component: CitaComponent },
      { path: 'cita-registro', component: CitaRegistroComponent },
      { path: 'cita-detalle/:id', component: CitaDetalleComponent },
      
    ],
    canActivate: [GuardPages]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
