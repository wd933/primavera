import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

import { ApiService } from '../../services/api.service';
import { AuthService } from '../../services/auth.service';
import { LoadingService } from '../../services/loading.service';

import { SpinnerDialog } from '@ionic-native/spinner-dialog/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  public login: number = 1;
  public formLogin: any;
  private fecha: string = '';

  constructor(
    private spinnerDialog: SpinnerDialog,
    private route: ActivatedRoute,
    private alertController: AlertController,
    private toastController: ToastController,
    private apiService: ApiService,
    private authService: AuthService,
    private loadingService: LoadingService,
    private navCtrl: NavController,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.formLogin = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  submitLogin() {
    if (this.formLogin.valid) {
      // this.loadingService.present();
      this.spinnerDialog.show(null, "Cargando...", true);
      this.apiService.post('/login', this.formLogin.getRawValue())
        .subscribe(
          async data => {
            // this.loadingService.dismiss();
            this.spinnerDialog.hide();
            if (data['success'] == 1) {
              this.authService.setLogin(data['data']);
              const toast = await this.toastController.create({
                message: 'Bienvenido.',
                duration: 2000
              });
              toast.present();
              this.navCtrl.navigateRoot('/home');
              //location.reload();
            } else {
              const toast = await this.toastController.create({
                message: 'Datos incorrectos.',
                duration: 2000
              });
              toast.present();
            }
          },
          error => {
            // this.loadingService.dismiss();
            this.spinnerDialog.hide();
            console.log('HTTP message:', error.message);
            console.log('HTTP name:', error.name);
            console.log('HTTP ok:', error.ok);
            console.log('HTTP status:', error.status);
            console.log('HTTP statusText:', error.statusText);
            console.log('HTTP url:', error.url);
            console.log('oops', error);
          }
        );
    }
  }

}
