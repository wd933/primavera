import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DatePicker } from '@ionic-native/date-picker/ngx';
import { NavController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

import { ApiService } from '../../services/api.service';
import { AuthService } from '../../services/auth.service';
import { LoadingService } from '../../services/loading.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {

  public formRegister: FormGroup;
  public myDate: string;
  public dpDate: Date = new Date();
  public dpShow: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private alertController: AlertController,
    private toastController: ToastController,
    private apiService: ApiService,
    private authService: AuthService,
    private loadingService: LoadingService,
    private navCtrl: NavController,
    private fb: FormBuilder,
    private datePicker: DatePicker
  ) { }

  ngOnInit() {
    this.formRegister = this.fb.group({
      tipo_doc: ['', [Validators.required]],
      n_documento: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      apellido_pa: ['', [Validators.required]],
      apellido_ma: ['', [Validators.required]],
      fecha_nacimiento: ['', [Validators.required]],
      sexo: ['', [Validators.required]],
      celular: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5)]],
      re_password: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  back() {
    history.back();
  }

  showDatepicker() {
    if (this.dpShow == false) {
      this.dpShow = true;
      this.datePicker.show({
        date: this.dpDate,
        mode: 'date',
        androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
        maxDate: new Date().valueOf(),
      }).then(
        date => {
          this.dpDate = date;
          this.formRegister.patchValue({
            fecha_nacimiento: this.zero(date.getDate()) + "/" + this.zero(date.getMonth() + 1) + "/" + date.getFullYear()
          });
        },
        err => {
          console.log('Error occurred while getting date: ', err);
        }
      ).finally(() => {
        this.dpShow = false;
      });
    }
  }

  zero(numero) {
    return (numero < 10 ? '0' : '') + numero;
  }

  async submitRegister() {
    if (this.formRegister.valid) {
      this.loadingService.present();
      this.apiService.post('/paciente/registro', this.formRegister.getRawValue())
        .subscribe(
          async data => {
            this.loadingService.dismiss();
            if (data['success'] == 1) {
              this.authService.setLogin(data['data']);
              const toast = await this.toastController.create({
                message: 'Bienvenido.',
                duration: 2000
              });
              toast.present();
              this.navCtrl.navigateRoot('/home');
            } else {
              const alert = await this.alertController.create({
                header: 'Importante',
                message: data['data'].join("<br>"),
                buttons: ['OK']
              });
              alert.present();
            }
          },
          error => {
            this.loadingService.dismiss();
            console.log('HTTP message:', error.message);
            console.log('HTTP name:', error.name);
            console.log('HTTP ok:', error.ok);
            console.log('HTTP status:', error.status);
            console.log('HTTP statusText:', error.statusText);
            console.log('HTTP url:', error.url);
            console.log('oops', error);
          }
        );
    }
  }
}
