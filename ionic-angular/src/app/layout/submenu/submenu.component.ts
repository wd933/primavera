import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-submenu',
  templateUrl: './submenu.component.html',
})
export class SubmenuComponent implements OnInit {

  public appPages = [
    {
      title: 'Inicio',
      url: '/home',
      icon: 'home'
    },{
      title: 'Citas',
      url: '/cita',
      icon: 'calendar'
    },{
      title: 'Médicos',
      url: '/medico',
      icon: 'people'
    },{
      title: 'Sedes',
      url: '/sede',
      icon: 'pin'
    }
  ];
  
  constructor(
    private authService: AuthService,
    private router: Router,
    private platform: Platform
  ) { }

  ngOnInit() {}

  logout() {
    this.authService.logout();
    this.router.navigate(['/']);
    //window.history.go(-(window.history.length-1));
  }

}
