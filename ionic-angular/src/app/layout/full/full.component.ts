import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-full',
  templateUrl: './full.component.html',
})
export class FullComponent implements OnInit {

  constructor(
    private platform: Platform
  ) { }

  ngOnInit() {}
}
