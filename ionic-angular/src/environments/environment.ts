// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // apiUrl: 'http://clinicamedicaprimavera.com/sys/api',
  apiUrl: 'http://192.168.1.16:81/laravel-citas/web/public_html/api',
  apiAuth: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjM5ZmQ0MDg5YWNhNzJlNWY5OTIyM2M2NTVjYmY0MjUzZDk3M2I5ZWRjMzhmNjc2NDgxYzgwZDdiZTAzOTIyYTU5NDY5YmM2NDMwZDBlZWE2In0.eyJhdWQiOiI0IiwianRpIjoiMzlmZDQwODlhY2E3MmU1Zjk5MjIzYzY1NWNiZjQyNTNkOTczYjllZGMzOGY2NzY0ODFjODBkN2JlMDM5MjJhNTk0NjliYzY0MzBkMGVlYTYiLCJpYXQiOjE1NDk2NjQyNTUsIm5iZiI6MTU0OTY2NDI1NSwiZXhwIjoxNTgxMjAwMjU1LCJzdWIiOiI0MDUiLCJzY29wZXMiOltdfQ.pg2jIzbZGSg-prBMx1KaBn_xu5mUEZf0F2GajUQMwYXtaffvRSLGhLK5hHnz46783aJr9Q7ZMxcW8Qul_P2noBPorKuXVfU1c0_SiOkC98rkcM48DQ9CjXbA9yOmBpA-_6bXuhalY7LHiuPE0BFpOC4pG0GbuMIjHtXrBjf8M0-5FQy9vLBy3Cuyt2JlPtoJZUYe3Ms_f1VOkMQbCyufSPCyRnwub0Xukkz8DFAl-wy0RrGJvuFwPxUriLr1JPQmmFlsVTmXMPsKk3-3wVlKOVnpKEyGOIjgJiMV8EUvNfhlbnVMQMfIXo1cZH2Pc0MLUnJkM-00UKUFmcfhDBXz6QYT7TwHSn43dLxiBLcjCrAwJB4Uq00YSCu0Z_rdsXeujLW99sV4N5aou3k5yhwSCKVzdJPDVdMZJDKduvtEA7LMXWSEm27YgmCDsSguWWuhuM_8oa_59cACu0GswA-ShGQHs-GRqwRUs_UCN3GIm-3OYlSWd30B74ahzysHkM8Vij8pLNp-aG1rS28XQ7blMyWP13gepEF3ZRXhmh1_YmQG5PyEbvvCR9-9O9bgHTCTnwM4T7yF_SLMo1XchPlYGTQQP-R1IYb8CgIRXckPctAt9N2KFlEOwUqDpt2qhBuZvgRWlhY8sxSJEhaWGITyY8Tps1cByEflBg_nUV7da9w'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
